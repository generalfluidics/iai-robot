import os, re, sys, inspect, datetime, time
import serial
import serial.tools.list_ports
import queue
import logging
import threading
import tkinter as tk
from tkinter.scrolledtext import ScrolledText
from tkinter import ttk, VERTICAL, HORIZONTAL, N, S, E, W, NE, SE, NW, SW, DISABLED, messagebox, END
import numpy as np

# Versions
codeVersion_Major = 1
codeVersion_Minor = 0
codeVersion_Build = 5

logger = logging.getLogger(__name__)

# TRACE_ONLY Set to Flase during normal operation
# Set to True for tracing in code, but no moves
TRACE_ONLY = False
SHOW_HEX = 0
VERBOSE = 0
LIMIT_SWITCH_VERBOSE = False
SKIP_HOME = False
SUCCESS_CODE = 0x64

sizeOfEachMessageFromTMCL = 9
buffersize = 50

# GUI Constants
APP_WIDTH = 1200
APP_HEIGHT = 680                
TEXT_WINDOW_HEIGHT = 11
DEBUG_CMD_FRAME_HEIGHT = 480

# Motor and Pump Constants
# IVEK DP 11 Displacement Pump
# 300 uL volume with 20 Lead Screw
# provides 0.2 micro liters for each 
# full step of the IVEK pump motor

MICROLITER_PER_FULL_STEP = 0.2
MICROSTEPS_PER_STEP = 256
MICROSTEPS_PER_MICROLITER  = 1280

# Sets Speed Range and Acceleration (Ramp) Range
# See TMCM-1140 Hardware Manual Table 9.1
# for Usrs, pulse divisor, and ramp divisor

Usrs = 8   
PULSE_DIVISOR = 0x00
RAMP_DIVISOR = 0x04
SYSTEM_CLOCK_FREQUENCY = 16000000

# Move Directoin Convention
MOVE_DIRECTION_ASPIRATE = 1
MOVE_DIRECTION_DISPENSE = -1

HOME_MOTOR_WAIT = 5.0

# Parameters for getting Digital Input Values
GIO_Input0 = 0
GIO_Input2 = 2
GIO_Input3 = 3

# Initial Settings for move dialog values
INIT_PF_ASP_VOL = '200'
INIT_PF_ASP_SPEED = '2000'
INIT_PF_ASP_RAMP = '400'
INIT_PF_ASP_DWELL = '0.0'

INIT_PF_DIS_VOL = '200'
INIT_PF_DIS_SPEED = '2000'
INIT_PF_DIS_RAMP = '400'
INIT_PF_DIS_DWELL = '0.0'

INIT_ASP_VOL = '150'
INIT_ASP_SPEED = '1500'
INIT_ASP_RAMP = '300'
INIT_ASP_DWELL = '0.0'

INIT_DIS_VOL = '150'
INIT_DIS_SPEED = '1500'
INIT_DIS_RAMP = '300'
INIT_DIS_DWELL = '0.0'


INIT_MR_VOL = '200'
INIT_MR_SPEED = '2000'
INIT_MR_RAMP = '400'
INIT_MR_DWELL = '0.0'

INIT_AG_VOL = '10'
INIT_AG_SPEED = '500'
INIT_AG_RAMP = '500'
INIT_AG_DWELL = '0.0'




#############################################################################################
#     #                 
##   ##   ##   # #    # 
# # # #  #  #  # ##   # 
#  #  # #    # # # #  # 
#     # ###### # #  # # 
#     # #    # # #   ## 
#     # #    # # #    # 

class control:

    f_counter_1000Hz_to_100Hz = 0
    f_counter_1000Hz_to_20Hz = 0
    f_counter_1000Hz_to_1Hz = 0
    f_counter_secondsCounterTimeref = 0
    msec_timer_tick = 0

    open_port_msg_count = 0

    initialized = False      

    killedProgram = False

    def taskLoop():

        # millisecond timer tick used for timing in pumpcontrol
        # pumpcontrol.setMoveTimeOutPeriod() sets this timer value before each motor move and it is decremented here.
        # This ensures that the pumpControl does no go back to the IDLE state (clearing the busy line) while the motor is still moving.
        control.msec_timer_tick -= 1

        # Set msec_timer_tick to zero if it goes negative.
        if (control.msec_timer_tick < 0): 
            control.msec_timer_tick = 0

 
        ####################################
        # 100 Hz Loop
        control.f_counter_1000Hz_to_100Hz += 1
        if (control.f_counter_1000Hz_to_100Hz >= 10):
            control.f_counter_1000Hz_to_100Hz = 0
            # This timeslice not used at present
          
        ####################################
        # 20 Hz Loop
        control.f_counter_1000Hz_to_20Hz += 1
        if (control.f_counter_1000Hz_to_20Hz >= 50):
            control.f_counter_1000Hz_to_20Hz = 0
            # in the loop we check for a com port. if it is different than previous, then we reset it
            comPortControl.manageCommunicationPorts()

        ####################################
        # 1 Hz Loop
        control.f_counter_1000Hz_to_1Hz += 1
        if (control.f_counter_1000Hz_to_1Hz >= 1000):
            control.f_counter_1000Hz_to_1Hz = 0
            control.f_counter_secondsCounterTimeref += 1
            if (comPortControl.checkIfComPortIsInUse()):
                if (control.initialized == False): 
                    pumpControl.initalizeMotorController()
                    pumpControl.autoPopulateCodeControlledEntryBoxes()
                    control.initialized = True

                # Check left limit switch state and update GUI
                pumpControl.Check_Left_Limit_Switch_Status()

                # Examine each of the three digital inputs from the TT Robot
                pumpControl.Check_Auto_Input()
                   
            else:
                control.showPortNotOpeninDebug()


        # Freewheeling behaviors go here if needed

        root.after(1, control.taskLoop)  # reschedule event in 1 ms

    def timeRefReset():
        control.f_counter_secondsCounterTimeref = 0

    def timeRefCheck():
        return control.f_counter_secondsCounterTimeref

    def on_closing():
        # kills the program gracefully
        control.stopProgram()

    def stopProgram():
        if (control.killedProgram == False):
            if (comPortControl.checkIfComPortIsInUse() == True):
                comPortControl.i_serialObject.close()
            print("Destroying")
            consoleConfiguration.quit()
            print("Thread and Gui Killed")
            control.killedProgram = True
        exit()

    def debugControl(data, typeKey = "none"):
        level = logging.INFO
        if (typeKey == "hex"):
            # convert the data to a hex to send
            hexString = []
            for index in range(0,len(data)):
                hexString.append(hex(data[index]))
            string = ' '.join(hexString)
            logger.log(level, str(string))

            # only put commands to command line window too, not other stuff
            # print(str(string))
        else:
            # keep as is
            logger.log(level, str(data))

    def showPortNotOpeninDebug():
        if (control.open_port_msg_count == 0):
            control.debugControl(" *****************************************")
            control.debugControl(" ")
            control.debugControl(" COM PORT MAY BE IN USE BY ANOTHER PROGRAM")
            control.debugControl("          OR USB CABLE NOT CONNECTED      ")
            control.debugControl(" ")
            control.debugControl(" CORRECT ISSUE; CLOSE PROGRAM; TRY AGAIN  ")    
            control.debugControl(" ")        
            control.debugControl(" *****************************************")

            control.open_port_msg_count += 1


#############################################################################################
######                                                               
#     # # #    #  ####     #####  #    # ###### ###### ###### #####  
#     # # ##   # #    #    #    # #    # #      #      #      #    # 
######  # # #  # #         #####  #    # #####  #####  #####  #    # 
#   #   # #  # # #  ###    #    # #    # #      #      #      #####  
#    #  # #   ## #    #    #    # #    # #      #      #      #   #  
#     # # #    #  ####     #####   ####  #      #      ###### #    # 

class RingBuffer:
    def __init__(self):
        self.data = [None for i in range(buffersize)]
        self.newMsgs = 0

    def append(self, x):
        self.data.pop(0)
        self.data.append(x)
        self.newMsgs += 1

    def get(self):
        msgRtn = self.newMsgs
        self.newMsgs -= 1
        return msgRtn, self.data

    def getCurrentCommand(self):
        msgRtn = self.newMsgs
        return msgRtn, self.data

    def checkForMsg(self):
        if (self.newMsgs > 0):
            return True
        else:
            return False
        
#############################################################################################
######                           #####                                           
#     # #    # #    # #####     #     #  ####  #    # ##### #####   ####  #      
#     # #    # ##  ## #    #    #       #    # ##   #   #   #    # #    # #      
######  #    # # ## # #    #    #       #    # # #  #   #   #    # #    # #      
#       #    # #    # #####     #       #    # #  # #   #   #####  #    # #      
#       #    # #    # #         #     # #    # #   ##   #   #   #  #    # #      
#        ####  #    # #          #####   ####  #    #   #   #    #  ####  ###### 

class pumpControl:

    i_ser = 0
    commandQueue = RingBuffer()
    waitingForResponse = False
    initializedMotor = False
    busyToggleStatus = 0

    error_check_enabled = False
    dump_calc_vals = False
    manual_move_in_progress = False
    show_cmds = False

    lastState = 0
    IDLE_STATE = 0

    lastDirectionMixRinse = MOVE_DIRECTION_DISPENSE

    defaultValuesForMC_ASP = {
    'Speed'     :  INIT_ASP_SPEED,
    'Volume'    :  INIT_ASP_VOL,
    'Ramp'      :  INIT_ASP_RAMP,   
    'Dwell'     :  INIT_ASP_DWELL,
    }

    defaultValuesForMC_DIS = {
    'Speed'     :  INIT_DIS_SPEED,
    'Volume'    :  INIT_DIS_VOL,
    'Ramp'      :  INIT_DIS_RAMP,
    'Dwell'     :  INIT_DIS_DWELL,
    }

    defaultValuesForMC_PF_ASP = {
    'Speed'     :  INIT_PF_ASP_SPEED,
    'Volume'    :  INIT_PF_ASP_VOL,
    'Ramp'      :  INIT_PF_ASP_RAMP,
    'Dwell'     :  INIT_PF_ASP_DWELL,
    }

    defaultValuesForMC_PF_DIS = {
    'Speed'     :  INIT_PF_DIS_SPEED,
    'Volume'    :  INIT_PF_DIS_VOL,
    'Ramp'      :  INIT_PF_DIS_RAMP,
    'Dwell'     :  INIT_PF_DIS_DWELL,
    }

    defaultValuesForMC_MR = {
    'Speed'     :  INIT_MR_SPEED,
    'Volume'    :  INIT_MR_VOL,
    'Ramp'      :  INIT_MR_RAMP,
    'Dwell'     :  INIT_MR_DWELL,
    }

    defaultValuesForMC_AG = {
    'Speed'     :  INIT_AG_SPEED,
    'Volume'    :  INIT_AG_VOL,
    'Ramp'      :  INIT_AG_RAMP,
    'Dwell'     :  INIT_AG_DWELL,
    }

    maxValuesForMC = {
    'Speed'     :    2047,
    'Volume'    :    300,
    'Ramp'      :    2047,
    'Dwell'     :    10.0,
    }    

    minValuesForMC = {
    'Speed'     :    1,
    'Volume'    :    1,
    'Ramp'      :    1,
    'Dwell'     :    0.0,
    }


    tmclStatusCodes = {
    100   :    "Command Success",
    101   :    "Command Loaded into EEPROM",
    1     :    "Wrong Checksum",
    2     :    "Invalid Command",
    3     :    "Wrong Type",
    4     :    "Invalid Value",
    5     :    "Configuration EEPROM Locked",
    6     :    "Command Not Available",
    }

    def setPySerialObject(objectVal):
        pumpControl.i_ser = objectVal

    def Byte2Decimal(b3, b2, b1, b0):
        rtnValue = b0 + ((b1 << 8) & 0x0000ff00) + ((b2 << 16) & 0x00ff0000) + ((b3 << 24) & 0xff000000) 
        return (rtnValue)

    def ClearCheckBoxes():
        io3StatusBoxvar.set(0)
        io2StatusBoxvar.set(0)
        io1StatusBoxvar.set(0)
        LSCheckboxvar.set(0)  
        BusyStatusBoxvar.set(0)     

    def setSpeedString_inuLPerS(whichBox):
        if (whichBox == "PRIME / FLUSH ASPIRATE"):
            try:
                speedVal = int(primeFlushAspirateInput_Speed.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                primeFlushAspirateInput_Speed.delete(0, END)
                primeFlushAspirateInput_Speed.insert(0, speedInput)
                
                # Use speed in units of [0..2047] to find speed in uL per sec units
                ts = '{0:5.2f}'.format(pumpControl.calculateSpeed_in_uL_per_sec(speedInput))
                primeFlushAspirateInput_SpeedCalculated.delete(0,END)
                primeFlushAspirateInput_SpeedCalculated.insert(0,ts)
            except ValueError:
                primeFlushAspirateInput_SpeedCalculated.delete(0,END)
                primeFlushAspirateInput_SpeedCalculated.insert(0,"N/A")

        elif (whichBox == "PRIME / FLUSH DISPENSE"):
            try:
                speedVal = int(primeFlushDispenseInput_Speed.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                primeFlushDispenseInput_Speed.delete(0, END)
                primeFlushDispenseInput_Speed.insert(0, speedInput)
                
                # Use speed in units of [0..2047] to find speed in uL per sec units
                ts = '{0:5.2f}'.format(pumpControl.calculateSpeed_in_uL_per_sec(speedInput))
                primeFlushDispenseInput_SpeedCalculated.delete(0,END)
                primeFlushDispenseInput_SpeedCalculated.insert(0,ts)
            except ValueError:
                primeFlushDispenseInput_SpeedCalculated.delete(0,END)
                primeFlushDispenseInput_SpeedCalculated.insert(0,"N/A")
                
        elif (whichBox == "ASPIRATE"):
            try:
                speedVal = int(aspirateInput_Speed.get())
                
                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                aspirateInput_Speed.delete(0, END)
                aspirateInput_Speed.insert(0, speedInput)

                # Use speed in units of [0..2047] to find speed in uL per sec units
                ts = '{0:5.2f}'.format(pumpControl.calculateSpeed_in_uL_per_sec(speedInput))
                aspirateInput_SpeedCalculated.delete(0,END)
                aspirateInput_SpeedCalculated.insert(0,ts)
            except ValueError:
                aspirateInput_SpeedCalculated.delete(0,END)
                aspirateInput_SpeedCalculated.insert(0,"N/A")
                
        elif (whichBox == "DISPENSE"):
            try:
                speedVal = int(dispenseInput_Speed.get())
                
                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                dispenseInput_Speed.delete(0, END)
                dispenseInput_Speed.insert(0, speedInput)

                # Use speed in units of [0..2047] to find speed in uL per sec units
                ts = '{0:5.2f}'.format(pumpControl.calculateSpeed_in_uL_per_sec(speedInput))
                dispenseInput_SpeedCalculated.delete(0,END)
                dispenseInput_SpeedCalculated.insert(0,ts)
            except ValueError:
                dispenseInput_SpeedCalculated.delete(0,END)
                dispenseInput_SpeedCalculated.insert(0,"N/A")
                
        elif (whichBox == "RINSE / WASH"):
            try:
                speedVal = int(mixRinseInput_Speed.get())
                
                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                mixRinseInput_Speed.delete(0, END)
                mixRinseInput_Speed.insert(0, speedInput)

                # Use speed in units of [0..2047] to find speed in uL per sec units
                ts = '{0:5.2f}'.format(pumpControl.calculateSpeed_in_uL_per_sec(speedInput))
                mixRinseInput_SpeedCalculated.delete(0,END)
                mixRinseInput_SpeedCalculated.insert(0,ts)
            except ValueError:
                mixRinseInput_SpeedCalculated.delete(0,END)
                mixRinseInput_SpeedCalculated.insert(0,"N/A")
                
        elif (whichBox == "AIR GAP"):
            try:
                speedVal = int(airgapInput_Speed.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                airgapInput_Speed.delete(0, END)
                airgapInput_Speed.insert(0, speedInput)

                # Use speed in units of [0..2047] to find speed in uL per sec units
                ts = '{0:5.2f}'.format(pumpControl.calculateSpeed_in_uL_per_sec(speedInput))
                airgapInput_SpeedCalculated.delete(0,END)
                airgapInput_SpeedCalculated.insert(0,ts)
            except ValueError:
                airgapInput_SpeedCalculated.delete(0,END)
                airgapInput_SpeedCalculated.insert(0,"N/A")        

    def setRamp_inPercentage(whichBox):
        if (whichBox == "PRIME / FLUSH ASPIRATE"):
            try:
                speedVal = int(primeFlushAspirateInput_Speed.get())
                rampVal = int(primeFlushAspirateInput_ramp.get())
                volume = int(primeFlushAspirateInput_Volume.get())
                dwell = float(primeFlushAspirateInput_dwell.get())

                # Check parameter ranges and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                primeFlushAspirateInput_Speed.delete(0, END)
                primeFlushAspirateInput_Speed.insert(0, speedInput)

                rampInput = pumpControl.checkInputForRange("Ramp", rampVal)
                primeFlushAspirateInput_ramp.delete(0, END)
                primeFlushAspirateInput_ramp.insert(0, rampInput)

                volumeInput = pumpControl.checkInputForRange("Volume", volume)
                primeFlushAspirateInput_Volume.delete(0, END)
                primeFlushAspirateInput_Volume.insert(0, volumeInput)

                # dwellInput = pumpControl.checkInputForRange("Dwell", dwell)
                # primeFlushAspirateInput_dwell.delete(0, END)
                # primeFlushAspirateInput_dwell.insert(0, dwellInput)

                ts = '{0:5.2f}'.format(pumpControl.calculateRamp_in_Percentage(volumeInput, speedInput, rampInput))
                primeFlushAspirateInput_rampCalculated.delete(0,END)
                primeFlushAspirateInput_rampCalculated.insert(0,ts)
            except ValueError:
                primeFlushAspirateInput_rampCalculated.delete(0,END)
                primeFlushAspirateInput_rampCalculated.insert(0,"N/A")
                
        elif (whichBox == "PRIME / FLUSH DISPENSE"):
            try:
                speedVal = int(primeFlushDispenseInput_Speed.get())
                rampVal = int(primeFlushDispenseInput_ramp.get())
                volume = int(primeFlushDispenseInput_Volume.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                primeFlushAspirateInput_Speed.delete(0, END)
                primeFlushAspirateInput_Speed.insert(0, speedInput)

                rampInput = pumpControl.checkInputForRange("Ramp", rampVal)
                primeFlushDispenseInput_ramp.delete(0, END)
                primeFlushDispenseInput_ramp.insert(0, rampInput)

                volumeInput = pumpControl.checkInputForRange("Volume", volume)
                primeFlushDispenseInput_Volume.delete(0, END)
                primeFlushDispenseInput_Volume.insert(0, volumeInput)

                ts = '{0:5.2f}'.format(pumpControl.calculateRamp_in_Percentage(volumeInput, speedInput, rampInput))
                primeFlushDispenseInput_rampCalculated.delete(0,END)
                primeFlushDispenseInput_rampCalculated.insert(0,ts)
            except ValueError:
                primeFlushDispenseInput_rampCalculated.delete(0,END)
                primeFlushDispenseInput_rampCalculated.insert(0,"N/A")
                
        elif (whichBox == "ASPIRATE"):
            try:
                speedVal = int(aspirateInput_Speed.get())
                rampVal = int(aspirateInput_ramp.get())
                volume = int(aspirateInput_Volume.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                aspirateInput_Speed.delete(0, END)
                aspirateInput_Speed.insert(0, speedInput)

                rampInput = pumpControl.checkInputForRange("Ramp", rampVal)
                aspirateInput_ramp.delete(0, END)
                aspirateInput_ramp.insert(0, rampInput)

                volumeInput = pumpControl.checkInputForRange("Volume", volume)
                aspirateInput_Volume.delete(0, END)
                aspirateInput_Volume.insert(0, volumeInput)

                ts = '{0:5.2f}'.format(pumpControl.calculateRamp_in_Percentage(volumeInput, speedInput, rampInput))
                aspirateInput_rampCalculated.delete(0,END)
                aspirateInput_rampCalculated.insert(0,ts)
            except ValueError:
                aspirateInput_rampCalculated.delete(0,END)
                aspirateInput_rampCalculated.insert(0,"N/A")
                
        elif (whichBox == "DISPENSE"):
            try:
                speedVal = int(dispenseInput_Speed.get())
                rampVal = int(dispenseInput_ramp.get())
                volume = int(dispenseInput_Volume.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                dispenseInput_Speed.delete(0, END)
                dispenseInput_Speed.insert(0, speedInput)

                rampInput = pumpControl.checkInputForRange("Ramp", rampVal)
                dispenseInput_ramp.delete(0, END)
                dispenseInput_ramp.insert(0, rampInput)

                volumeInput = pumpControl.checkInputForRange("Volume", volume)
                dispenseInput_Volume.delete(0, END)
                dispenseInput_Volume.insert(0, volumeInput)

                ts = '{0:5.2f}'.format(pumpControl.calculateRamp_in_Percentage(volumeInput, speedInput, rampInput))
                dispenseInput_rampCalculated.delete(0,END)
                dispenseInput_rampCalculated.insert(0,ts)
            except ValueError:
                dispenseInput_rampCalculated.delete(0,END)
                dispenseInput_rampCalculated.insert(0,"N/A")
                
        elif (whichBox == "RINSE / WASH"):
            try:
                speedVal = int(mixRinseInput_Speed.get())
                rampVal = int(mixRinseInput_ramp.get())
                volume = int(mixRinseInput_Volume.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                mixRinseInput_Speed.delete(0, END)
                mixRinseInput_Speed.insert(0, speedInput)

                rampInput = pumpControl.checkInputForRange("Ramp", rampVal)
                mixRinseInput_ramp.delete(0, END)
                mixRinseInput_ramp.insert(0, rampInput)

                volumeInput = pumpControl.checkInputForRange("Volume", volume)
                mixRinseInput_Volume.delete(0, END)
                mixRinseInput_Volume.insert(0, volumeInput)

                ts = '{0:5.2f}'.format(pumpControl.calculateRamp_in_Percentage(volumeInput, speedInput, rampInput))
                mixRinseInput_rampCalculated.delete(0,END)
                mixRinseInput_rampCalculated.insert(0,ts)
            except ValueError:
                mixRinseInput_rampCalculated.delete(0,END)
                mixRinseInput_rampCalculated.insert(0,"N/A")
                
        elif (whichBox == "AIR GAP"):
            try:
                speedVal = int(airgapInput_Speed.get())
                rampVal = int(airgapInput_ramp.get())
                volume = int(airgapInput_Volume.get())

                # Check parameter range and update if it is out of range
                speedInput = pumpControl.checkInputForRange("Speed", speedVal)
                airgapInput_Speed.delete(0, END)
                airgapInput_Speed.insert(0, speedInput)

                rampInput = pumpControl.checkInputForRange("Ramp", rampVal)
                airgapInput_ramp.delete(0, END)
                airgapInput_ramp.insert(0, rampInput)

                volumeInput = pumpControl.checkInputForRange("Volume", volume)
                airgapInput_Volume.delete(0, END)
                airgapInput_Volume.insert(0, volumeInput)

                ts = '{0:5.2f}'.format(pumpControl.calculateRamp_in_Percentage(volumeInput, speedInput, rampInput))
                airgapInput_rampCalculated.delete(0,END)
                airgapInput_rampCalculated.insert(0,ts)
            except ValueError:
                airgapInput_rampCalculated.delete(0,END)
                airgapInput_rampCalculated.insert(0,"N/A")

    def autoPopulateCodeControlledEntryBoxes():
        pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH ASPIRATE")
        pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH DISPENSE")
        pumpControl.setSpeedString_inuLPerS("ASPIRATE")
        pumpControl.setSpeedString_inuLPerS("DISPENSE")
        pumpControl.setSpeedString_inuLPerS("RINSE / WASH")
        pumpControl.setSpeedString_inuLPerS("AIR GAP")

        pumpControl.setRamp_inPercentage("PRIME / FLUSH ASPIRATE")
        pumpControl.setRamp_inPercentage("PRIME / FLUSH DISPENSE")
        pumpControl.setRamp_inPercentage("ASPIRATE")
        pumpControl.setRamp_inPercentage("DISPENSE")
        pumpControl.setRamp_inPercentage("RINSE / WASH")
        pumpControl.setRamp_inPercentage("AIR GAP")

    def initalizeMotorController():
        control.debugControl("Initalize the system")

        pumpControl.getFirmwareVersion()
        pumpControl.MaxCurrent()
        pumpControl.StandbyCurrent()
        pumpControl.RightSWDisable()
        pumpControl.LeftSWDisable()
        pumpControl.MinSpeed()
        pumpControl.RampMode()
        pumpControl.MicroStepResolution()
        pumpControl.SoftStopFlag()
        pumpControl.RampDivisor()
        pumpControl.PulseDivisor()
        pumpControl.EnableInterpolation()
        pumpControl.ReferenceSearchMode()
        pumpControl.ReferenceSearchSpeed()
        pumpControl.ReferenceSwitchSpeed()
        pumpControl.Boostcurrent()
        pumpControl.Freewheeling()
        pumpControl.PowerDownDelay()
        pumpControl.Pull_Up_On()
        pumpControl.Dig_Out_0_High_Not_Busy()
        pumpControl.Dig_Out_1_On()
        pumpControl.LimitSwitchPolarity()

        # Motor calculation functions are called here with fixed values
        # These can be compared with same values calculated in a Excel file
        # as a check of the calculations used in this software code.
        # Set dump = True to print the results to the terminal window.
        # The pulse divisor and ramp divisor also affect these calculations
        # they are fixed as constants near the top of this source file.

        test_volume = 100
        test_speed = 1000
        test_accel = 100
        dump = pumpControl.dump_calc_vals
        if (dump): print("===================================================================")
        pumpControl.calculateSpeed_in_uL_per_sec(test_speed, dump)
        pumpControl.calculateRamp_in_Percentage(test_volume, test_speed, test_accel, dump)
        pumpControl.calculateMotor_Move_Time(test_volume, test_speed, test_accel, dump)
        if (dump): print("===================================================================")

        pumpControl.initializedMotor = True

        pumpControl.ClearCheckBoxes()

    def calculateSpeed_in_uL_per_sec(speedval, dump = False):
    
        # See TMCM-1140 Hardware Manual Section 9 and Table 9-1 to
        # calculate Microstep Frequency
    
        # These settings are fixed constants 
        uL_per_full_step = MICROLITER_PER_FULL_STEP 
        microStepPerStep = MICROSTEPS_PER_STEP
        fClockHz = SYSTEM_CLOCK_FREQUENCY
        pulse_div = PULSE_DIVISOR

        # Calculate speed based on [0..2047] speedval input
        micro_step_per_sec = (fClockHz * speedval) / ((pow(2,pulse_div)) * 2048 * 32)
        full_step_per_sec = micro_step_per_sec / (pow(2,Usrs))
        speed_uL_per_sec = uL_per_full_step * full_step_per_sec

        if (dump == True):
            print('')
            print('Check Case for Volume = 100 uL; Speed = 1000; Ramp (accel) = 1000')
            print('')
            print('Calculate Speed in uL per sec')
            print('uSteps_per_Sec = {0:6.2f}'. format(micro_step_per_sec))
            print('full_step_per_sec = {0:6.2f}'.format(full_step_per_sec))
            print('speed_uL_per_sec = {0:6.2f}'.format(speed_uL_per_sec))

        return speed_uL_per_sec

    def calculateRamp_in_Percentage(volume, speedval, rampVal, dump = False):
        #
        # Find percentage of move that the ramp take to complete
        # Method: 
        # Find total distance of move: dTotal =  uStep per uLiter * volume (uL)
        # Find time of ramp: tRamp = V/A [Velocity / Acceleration]
        # Find distance of ramp: dRamp = V^2/2*A [Velocity^2/(2*Acceleration)]
        # Find Percentage of time for ramp in total move = (dRamp / dTotal) * 100
        #

        pulse_div = PULSE_DIVISOR
        ramp_div = RAMP_DIVISOR
        fClockHz = SYSTEM_CLOCK_FREQUENCY
        uL_per_full_step = MICROLITER_PER_FULL_STEP 

        accelRamp = (fClockHz**2) * rampVal / (2**(pulse_div+ramp_div+29))
        micro_step_per_sec = (fClockHz * speedval) / ((pow(2,pulse_div)) * 2048 * 32)
        full_step_per_sec = micro_step_per_sec / (pow(2,Usrs))
        speed_uL_per_sec = uL_per_full_step * full_step_per_sec
        
        dTotal = MICROSTEPS_PER_MICROLITER * volume
        tRamp = micro_step_per_sec / accelRamp
        dRamp = float(micro_step_per_sec**2) / float(2*accelRamp)

        # Calculate total Trapezoidal move time, 
        # where t1 is Ramp Up, t2 is constant speed, 
        # and t3 is ramp down
        #
        # IMPORTANT if tTrapMove is negative the ramps are too slow!!!
        #
        t1 = tRamp
        t2 = (dTotal - 2*dRamp) / micro_step_per_sec
        t3 = tRamp

        tTrapMove = t1+t2+t3

        # Ramp percentage is found using time of the acceleration (t1) 
        # divided by the total trapezoidal move time 
        ramp_percentage = ( t1 / tTrapMove) * 100

        if (dump == True):
            print('')
            print('Calc Acceleration Ramp in Percent')
            print('accel = {0:6.2f}'.format(accelRamp))
            print('uSteps_per_Sec = {0:6.2f}'. format(micro_step_per_sec))
            print('dTotal = {0:6.2f}'.format(dTotal))
            print('tRamp = {0:8.6f}'.format(tRamp))
            print('dRamp = {0:6.2f}'.format(dRamp))

            print('ramp_Percentage = {0:6.2f}'.format(ramp_percentage))

        return ramp_percentage
       

    def calculateMotor_Move_Time(volume, speedval, rampval, dump = False): 
     
        # Find total move time for the Trapezoidal move

        # Assign constants to parameters
        pulse_div = PULSE_DIVISOR
        ramp_div = RAMP_DIVISOR
        fClockHz = SYSTEM_CLOCK_FREQUENCY
        uL_per_full_step = MICROLITER_PER_FULL_STEP 

        # Calculate accleration 
        accelRamp = (fClockHz**2) * rampval / (2**(pulse_div+ramp_div+29))
        micro_step_per_sec = (fClockHz * speedval) / ((pow(2,pulse_div)) * 2048 * 32)

        # Convert move from volume to microsteps 
        # this is total distance for move     
        dTotal = MICROSTEPS_PER_MICROLITER * volume

        # Find time of reach constant velocity in micro steps per second
        # for the acceleration ramp
        tRamp = micro_step_per_sec / accelRamp

        # Fine distance traveled during the accel ramp section for the trapezoidal move
        # Equation is algebraic solution to the integral of velocity during the ramp.  
        # We assume accel and decel are symetrical
        # See programming notes.
        dRamp = (micro_step_per_sec**2) / (2*accelRamp)

        # Calculate total Trapezoidal move time, 
        # where t1 is Ramp Up, t2 is constant speed, 
        # and t3 is ramp down
        #
        # IMPORTANT if tTrapMove is negative the ramps are too slow!!!
        #
        t1 = tRamp
        t2 = (dTotal - 2*dRamp) / micro_step_per_sec
        t3 = tRamp

        # Total time of move here
        tTrapMove = t1+t2+t3


        if (dump == True):
            print('')
            print('calculateMotor_Move_Time')
            print('accel = {0:6.2f}'.format(accelRamp))
            print('uSteps_per_Sec = {0:6.2f}'. format(micro_step_per_sec))
            print('dTotal = {0:6.2f}'.format(dTotal))
            print('tRamp = {0:8.6f}'.format(tRamp))
            print('dRamp = {0:6.2f}'.format(dRamp))

            print('t1 = {0:8.6f}'.format(t1))
            print('t2 = {0:8.6f}'.format(t2))
            print('t3 = {0:8.6f}'.format(t3))
            print('Time of Trapezoidal Move = {0:8.6f}'.format(tTrapMove))
            print('')

        return tTrapMove      


    def jogRight(Speed):
        control.debugControl("jog right the motor")

        b4 = (Speed >> 24) & 0xff
        b3 = (Speed >> 16) & 0xff
        b2 = (Speed >> 8) & 0xff
        b1 = Speed & 0xff

        dataset = [0x01, 0x01, 0x00, 0x00, b4, b3, b2, b1]
        pumpControl.transmitDataNow(dataset)
        
    def jogLeft(Speed):
        control.debugControl("jog left the motor")

        b4 = (Speed >> 24) & 0xff
        b3 = (Speed >> 16) & 0xff
        b2 = (Speed >> 8) & 0xff
        b1 = Speed & 0xff

        dataset = [0x01, 0x02, 0x00, 0x00, b4, b3, b2, b1]
        pumpControl.transmitDataNow(dataset)

    def MoveRel(volume, speed, ramp, move_direction):
       
        if (TRACE_ONLY): return

        # Relative move uses both positive and negative microstep counts 
        # Positive microstep moves in the Aspirate direction
        # Negative microstep moves in the Dispense direction


        # Set speed based on the speed setting [0...2047]
        pumpControl.MaxPositionSpeed(speed)
        
        # Set acceleration based on the ramp setting [0...2047]
        pumpControl.MaxAcceleration(ramp)

        timeMove = pumpControl.calculateMotor_Move_Time(volume, speed, ramp)
        
        # Convert decimal volume into four byte values of microsteps for the move
        # the move_direction is positive for aspirate direction, and negative for dispense direction moves.
        # The np.int32 will put the negative values into Two's Compliment form which is needed by TMCM controller commands.
        relative_pos_in_usteps = np.int32(volume * MICROSTEPS_PER_MICROLITER * move_direction)

        control.debugControl("Move Relative Vol={0} Speed={1} Ramp={2} uSteps={3}".format(volume, speed, ramp, relative_pos_in_usteps))

        b4 = (relative_pos_in_usteps >> 24) & 0xff
        b3 = (relative_pos_in_usteps >> 16) & 0xff
        b2 = (relative_pos_in_usteps >> 8) & 0xff
        b1 = relative_pos_in_usteps & 0xff
            
        dataset = [0x01, 0x04, 0x01, 0x00, b4, b3, b2, b1]
        pumpControl.transmitDataNow(dataset)

        return timeMove
        
    def MaxPositionSpeed(speed):

        b2 = (speed >> 8) & 0xff
        b1 = speed & 0xff
        
        dataset = [0x01, 0x05, 0x04, 0x00, 0x00, 0x00, b2, b1]
        pumpControl.transmitDataNow(dataset)

    def MaxAcceleration(acceleration):
             
        #value = int(acceleration)
        b2 = (acceleration >> 8) & 0xff
        b1 = acceleration & 0xff
        dataset = [0x01, 0x05, 0x05, 0x00, 0x00, 0x00, b2, b1]

        pumpControl.transmitDataNow(dataset)

    def MaxCurrent():
        control.debugControl("Max Current")
        dataset = [0x01, 0x05, 0x06, 0x00, 0x00, 0x00, 0x00, 0x7F] # Set Max Current to 0.99 A RMS (0x7F)
        pumpControl.transmitDataNow(dataset)

    def StandbyCurrent():
        control.debugControl("Standby Current")
        dataset = [0x01, 0x05, 0x07, 0x00, 0x00, 0x00, 0x00, 0x08] # Set Standby Current to value = 8
        pumpControl.transmitDataNow(dataset)

    def RightSWDisable():
        control.debugControl("Right SW Disable")
        dataset = [0x01, 0x05, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x01] # Disable Right Limit Switch
        pumpControl.transmitDataNow(dataset)

    def LeftSWDisable():
        # We disable the left limit switch, even though we use the left limit switch to home motor
        control.debugControl("Left SW Disable")
        dataset = [0x01, 0x05, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x01] # Disable Left Limit Switch
        pumpControl.transmitDataNow(dataset)

    def MinSpeed():
        control.debugControl("Min Speed")
        dataset = [0x01, 0x05, 0x82, 0x00, 0x00, 0x00, 0x00, 0x01] # Sets min speed for idle
        pumpControl.transmitDataNow(dataset)

    def RampMode():
        control.debugControl("Ramp Mode")
        dataset = [0x01, 0x05, 0x8A, 0x00, 0x00, 0x00, 0x00, 0x00] # Set Ramp mode for moves
        pumpControl.transmitDataNow(dataset)

    def MicroStepResolution():
        control.debugControl("MicroStep Resolution")
        dataset = [0x01, 0x05, 0x8C, 0x00, 0x00, 0x00, 0x00, 0x08] # Set 256 microsteps per full step
        pumpControl.transmitDataNow(dataset)

    def SoftStopFlag():
        control.debugControl("Soft Stop Flag Set to 1 ")
        dataset = [0x01, 0x05, 0x95, 0x00, 0x00, 0x00, 0x00, 0x01] # Use soft stop when a limit switch is hit.
        pumpControl.transmitDataNow(dataset)

    def RampDivisor():
        control.debugControl("Ramp Divisor")
        dataset = [0x01, 0x05, 0x99, 0x00, 0x00, 0x00, 0x00, RAMP_DIVISOR] # Ramp divisor sets acceleration (pps/s units) for ramps
        pumpControl.transmitDataNow(dataset)

    def PulseDivisor():
        control.debugControl("Pulse Divisor")
        dataset = [0x01, 0x05, 0x9A, 0x00, 0x00, 0x00, 0x00, PULSE_DIVISOR] # Pulse divisor sets speed (microsteps per sec) range
        pumpControl.transmitDataNow(dataset)

    def EnableInterpolation():
        control.debugControl("Enable Interpolation")
        dataset = [0x01, 0x05, 0xA0, 0x00, 0x00, 0x00, 0x00, 0x00] # Set to zero; only applies if 16 microstepping is used
        pumpControl.transmitDataNow(dataset)

    def ReferenceSearchMode():
        control.debugControl("Reference Search Mode")
        dataset = [0x01, 0x05, 0xC1, 0x00, 0x00, 0x00, 0x00, 0x01] # Home search mode, 0x01 = Left L/S from one side only
        pumpControl.transmitDataNow(dataset)

    def ReferenceSearchSpeed():
        control.debugControl("Reference Search Speed")
        dataset = [0x01, 0x05, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xC8] # Speed used to home motor to the limit switch
        pumpControl.transmitDataNow(dataset)

    def ReferenceSwitchSpeed():
        control.debugControl("Reference Switch Speed")
        dataset = [0x01, 0x05, 0xC3, 0x00, 0x00, 0x00, 0x00, 0x32] # Speed used to find the switching point during home
        pumpControl.transmitDataNow(dataset)

    def Boostcurrent():
        control.debugControl("Boost Current")
        dataset = [0x01, 0x05, 0xC8, 0x00, 0x00, 0x00, 0x00, 0x00] # Current used during accelerations, when set to zero, max current settingis used.
        pumpControl.transmitDataNow(dataset)

    def Freewheeling():
        control.debugControl("Freewheeling")
        dataset = [0x01, 0x05, 0xCC, 0x00, 0x00, 0x00, 0x00, 0x64] # Time motor current is set off after a jog (units 10 ms): 100 x 10 ms         
        pumpControl.transmitDataNow(dataset)

    def PowerDownDelay():
        control.debugControl("Power Down Delay")
        dataset = [0x01, 0x05, 0xD6, 0x00, 0x00, 0x00, 0x00, 0x64] # Time motor current is set off after a move (units 10 ms): 100 x 10 ms
        pumpControl.transmitDataNow(dataset)
        
    def Pull_Up_On():
        control.debugControl("Configure Pull-Up R for Inputs (1,2,and 3)")
        dataset = [0x01, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01] # Use pull-up on TMCM-1140 for digital inputs
        pumpControl.transmitDataNow(dataset)

    def Pull_Up_Off():
        control.debugControl("Configure Pull-Up R for Inputs (1,2,and 3)")
        dataset = [0x01, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00] # Not used 
        pumpControl.transmitDataNow(dataset)
    
    def Dig_Out_0_Low_Busy():
        control.debugControl("Set Digital Output (OUT_O) Low (0V) On - Busy ")
        pumpControl.setOutputIO(1) 
    
        dataset = [0x01, 0x0E, 0x00, 0x02, 0x00, 0x00, 0x00, 0x01]
        pumpControl.transmitDataNow(dataset)
         
    def Dig_Out_0_High_Not_Busy():
        control.debugControl("Turn Digital Output (OUT_O) High (+24V) Off - Not Busy")
        pumpControl.setOutputIO(0)   
       
        dataset = [0x01, 0x0E, 0x00, 0x02, 0x00, 0x00, 0x00, 0x0]
        pumpControl.transmitDataNow(dataset)
 
    def Dig_Out_1_On():
        control.debugControl("Turn Digital Output 1 (OUT_1) to ON (+5V)")
        dataset = [0x01, 0x0E, 0x01, 0x02, 0x00, 0x00, 0x00, 0x01]
        pumpControl.transmitDataNow(dataset)

    def Dig_Out_1_Off():
        control.debugControl("Turn Digital Output 1 (OUT_1) to Off (0V)")
        dataset = [0x01, 0x0E, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00]
        pumpControl.transmitDataNow(dataset)

    def LimitSwitchPolarity():
        control.debugControl("Limit Switch Polarity set to Normal")
        dataset = [0x01, 0x0A, 0x4F, 0x00, 0x00, 0x00, 0x00, 0x00]
        pumpControl.transmitDataNow(dataset)

    def Check_Auto_Input():
        # Get status of the three digital inputs 
        # These inputs are set by the TT Robot 
        # to trigger pump commands
        #
        # These commands update the i3, i2, and i1 
        # checkboxes.
        #  
        # NOTE TMCM-1140 Input 1 is assigned to the left limit switch
        # so it is skipped here
        # 
        pumpControl.GIO_Get_Input_Status(0) #i1 on GUI
        pumpControl.GIO_Get_Input_Status(2) #i2 on GUI
        pumpControl.GIO_Get_Input_Status(3) #i3 on GUI

        # Recall the i3, i2, and i1 checkboxes
        #
        i3 = io3StatusBoxvar.get()
        i2 = io2StatusBoxvar.get()
        i1 = io1StatusBoxvar.get()

        # Check for input patterns and execute commands
        # Patterns below are three bit binary
        # i3 = most sig bit, i1 = least sig bit
        #
        # 000 = No Input
        # 001 = Prime/Flush Aspirate
        # 010 = Prime/Flush Dispense
        # 011 = Aspirate
        # 100 = Dispense
        # 101 = Mix/Rinse
        # 110 = Air Gap
        # 111 = Home pump to Left Limit Switch

        newState = (2**2)*i3 + (2**1)*i2 + (2**0)*i1
          
        # This clears the busy line in  AUTO mode
        # If inputs are all zero and the msec_timer_tick has counted down
        # we are ready to go back to the IDLE state and signal Robot that 
        # the pump move is completed
        if ( (i3 == 0) and (i2 == 0) and (i1 == 0) and (control.msec_timer_tick <= 0) ): 
            # Set Out 0 to the Not Busy state each time we are IDLE
            # Do it only once for each transition to the IDLE state
            if (pumpControl.lastState != pumpControl.IDLE_STATE):
                pumpControl.Dig_Out_0_High_Not_Busy()

            pumpControl.lastState = pumpControl.IDLE_STATE

        # This clears the Busy Line when a MANUAL move has occured and the move timer expires    
        if ( (pumpControl.manual_move_in_progress == True) and (control.msec_timer_tick <= 0) ): 
            pumpControl.Dig_Out_0_High_Not_Busy()
            pumpControl.manual_move_in_progress = False 

        # Only allow moves when the previous input pattern was 000 - IDLE_STATE
        if (pumpControl.lastState == pumpControl.IDLE_STATE): 

            # Assign new state for next iteration
            pumpControl.lastState = newState

            if ( (i3 == 0) and (i2 == 0) and (i1 == 1) ): pumpControl.primeFlushAspirateControl("AUTO")
            if ( (i3 == 0) and (i2 == 1) and (i1 == 0) ): pumpControl.primeFlushDispenseControl("AUTO")
            if ( (i3 == 0) and (i2 == 1) and (i1 == 1) ): pumpControl.aspirateControl("AUTO")
            if ( (i3 == 1) and (i2 == 0) and (i1 == 0) ): pumpControl.dispenseControl("AUTO")
            if ( (i3 == 1) and (i2 == 0) and (i1 == 1) ): pumpControl.mixRinseControl("AUTO")
            if ( (i3 == 1) and (i2 == 1) and (i1 == 0) ): pumpControl.airgapControl("AUTO")
            if ( (i3 == 1) and (i2 == 1) and (i1 == 1) ): pumpControl.HomeMotor("AUTO")


    def GIO_Get_Input_Status(input):
        #control.debugControl("Get Input Status")
        b2 = (input & 0xFF)
        dataset = [0x01, 0x0F, b2, 0x00, 0x00, 0x00, 0x00, 0x00]
        cmdReply = pumpControl.transmitDataNow(dataset)

        # b2 specifies general purpose input number (0, 2, or 3)
        # Input 1 is assigned to Left Limit Switch
        # cmdReply[7] byte responds input state with valuve [0,1]
        ioState = cmdReply[7]

        if (input == GIO_Input0):
            if (ioState == 1): io1StatusBoxvar.set(0)
            else:              io1StatusBoxvar.set(1)
        if (input == GIO_Input2):
            if (ioState == 1): io2StatusBoxvar.set(0)
            else:              io2StatusBoxvar.set(1)
        if (input == GIO_Input3):
            if (ioState == 1): io3StatusBoxvar.set(0)
            else:              io3StatusBoxvar.set(1)
    
    def Check_Left_Limit_Switch_Status():
        if (VERBOSE):     
            control.debugControl("Get Left Limit Switch Status = ")

        dataset = [0x01, 0x06, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00]
        cmdReply = pumpControl.transmitDataNow(dataset)

        if (LIMIT_SWITCH_VERBOSE): 
            control.debugControl(cmdReply, "hex")

        # Left limit switch is low when the "flag" is not inside of the "C" optical switch
        # The "flag" is part of the motor lead screw mechanism.
        # When the "flag" is inside the optical switch (i.e. at the limit switch) the 
        # optical switch output is pulled high by the pull up resistor installed in 
        # the Interconnect box wiring, independent of the pull ups in the motion controller.
        if (cmdReply[7] == 1): LSCheckboxvar.set(1)
        else:                  LSCheckboxvar.set(0)

    def getFirmwareVersion():
        control.debugControl("Get TMCM-1140 firmware version")

        # get firmware version wiht ASCII reply string
        # This is a special case; all other replies are in binary format
        dataset = [0x01, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        cmdReply = pumpControl.transmitDataNow(dataset)   

        # Enable error checking of return codes after receipt of version string
        # The version string does not follow the error coding convention like
        # all other commands.
        pumpControl.error_check_enabled = True

        control.debugControl(cmdReply)

    def GIO_Get_Output_Status(input):
        control.debugControl("Get Output Status")
        dataset = [0x01, 0x0F, (input & 0xFF), 0x02, 0x00, 0x00, 0x00, 0x00]
        pumpControl.transmitDataNow(dataset)
   
    # Not used at present
    def MoveAbs():
        dataset = []
        pumpControl.transmitDataNow(dataset)


    def setOutputIO(setVal):
        if (setVal > 1):
            setVal = 1
        elif (setVal < 0):
            setval = 0
        BusyStatusBoxvar.set(setVal)
    
    #def checkInputForRange(keyString, value, boxString): 
    def checkInputForRange(keyString, value):
        maxVal = pumpControl.maxValuesForMC[keyString]
        minVal = pumpControl.minValuesForMC[keyString]

        if (value > maxVal):
            tk.messagebox.showwarning(title = "Out of Range: " + str(keyString), message = "You have entered too high of a value for: " + str(keyString) +
                                      "\n Changed to the max value of " + str(maxVal) + ".") 
            return maxVal
        elif (value < minVal):
            tk.messagebox.showwarning(title = "Out of Range: " + str(keyString), message = "You have entered too low of a value for: " + str(keyString) +
                                      "\n Changed to the minimum value of " + str(minVal) + ".") 
            return minVal
        else:
            return value
               
    def setMoveTimeOutPeriod(moveTime, dwellTime):
        # Set msec_timer_tick (a count down timer) to the duration.
        # the control thread will decrement this timer every 1 millisecond
        # This timer determines when it is OK to clear the busy line to the TT Robot.

        # Find total duration in seconds and convert to milliseconds
        control.msec_timer_tick  = (moveTime + dwellTime) * 1000

        control.debugControl('MoveTimeOutPeriod(msec)= {0:6.2f} move time(s)= {1:4.2f} dwell time(s)= {2:4.2f}'.format(control.msec_timer_tick , moveTime, dwellTime))

    def setMoveType(movetype):
        if (movetype == "Manual"): 
            pumpControl.manual_move_in_progress = True
        else:                       
            pumpControl.manual_move_in_progress = False

    def HomeMotor(movetype = "Manual"):
        control.debugControl("========== Home motor to Left L/S ========== ")

        # Home function depends on: mode set to 1 (cmd 193); and OUT_1 set high (5V) 
        # Cmd 193 sets home to use look for left limit switch only
        # Output 1 (OUT_1) is used to power optical sensor in limit switch on IVEK Pump / Motor assembly.
        # Home Limit Switch is logic 1 when Not at Limit Switch and logic 0 when at Limit Switch
        # We home to the Left Limit Switch

        # Reference Speed Start command is used to home the motor
        dataset = [0x01, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        if (SKIP_HOME != True): 
             pumpControl.transmitDataNow(dataset) 
        else:
             control.debugControl("Home motor in TRACE mode - no move")    

        # Signal TT Robot Pump is Busy doing a move
        pumpControl.Dig_Out_0_Low_Busy()

        # We need to wait for motor to home before clearing the busy line to TT Robot
        # But we don't know how far the home move will be, so we pick a conservative 
        # Motor move time to ensure we reach home before clearing the busy line.
        control.debugControl("Home motor wait timer start")
        pumpControl.setMoveTimeOutPeriod(HOME_MOTOR_WAIT, 0)
        pumpControl.setMoveType(movetype)

        
    def stopMotor():
        control.debugControl("stop the motor")
        dataset = [0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        pumpControl.transmitDataNow(dataset)

        # Set Busy Line to Not Busy
        pumpControl.Dig_Out_0_High_Not_Busy()

        # Clear the manual move in progress, just in case Stop occurs during it.
        pumpControl.manual_move_in_progress = False
        
    def primeFlushAspirateControl(movetype = "Manual"):
        control.debugControl("=========== Prime/Flush Aspirate ===========")

        #Set defaults to zero
        volumeInput = 0
        speedInput = 0
        rampInput = 0
        
        #Make sure input is an integer
        try:
            volumeInput =   int(primeFlushAspirateInput_Volume.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Volume")
            primeFlushAspirateInput_Volume.delete(0, END)
            primeFlushAspirateInput_Volume.insert(0, pumpControl.defaultValuesForMC["Volume"])

        try:    
            speedInput =    int(primeFlushAspirateInput_Speed.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Speed")
            primeFlushAspirateInput_Speed.delete(0, END)
            primeFlushAspirateInput_Speed.insert(0, pumpControl.defaultValuesForMC["Speed"])
            
        try:    
            rampInput = int(primeFlushAspirateInput_ramp.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Ramp")
            primeFlushAspirateInput_ramp.delete(0, END)
            primeFlushAspirateInput_ramp.insert(0, pumpControl.defaultValuesForMC["Ramp"])
        
        try:
            dwellInput =    float(primeFlushAspirateInput_dwell.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an floating point value for: Dwell")
            primeFlushAspirateInput_dwell.delete(0, END)
            primeFlushAspirateInput_dwell.insert(0, pumpControl.defaultValuesForMC["Dwell"])
        
        #Update value based after checking range
        volumeInput = pumpControl.checkInputForRange("Volume", volumeInput)
        primeFlushAspirateInput_Volume.delete(0, END)
        primeFlushAspirateInput_Volume.insert(0, volumeInput)
        
        speedInput = pumpControl.checkInputForRange("Speed", speedInput)
        primeFlushAspirateInput_Speed.delete(0, END)
        primeFlushAspirateInput_Speed.insert(0, speedInput)
        
        rampInput = pumpControl.checkInputForRange("Ramp", rampInput)
        primeFlushAspirateInput_ramp.delete(0, END)
        primeFlushAspirateInput_ramp.insert(0, rampInput)
        
        dwellInput = pumpControl.checkInputForRange("Dwell", dwellInput)
        primeFlushAspirateInput_dwell.delete(0, END)
        primeFlushAspirateInput_dwell.insert(0, dwellInput)

        # 
        if (TRACE_ONLY == False): 
        
            # Signal TT Robot Pump is Busy doing a move
            pumpControl.Dig_Out_0_Low_Busy()

            # Move in the Aspirate Direction
            timeMove = pumpControl.MoveRel(volumeInput, speedInput, rampInput, MOVE_DIRECTION_ASPIRATE)

        # Set motor move time and add extra dwell (pause) time if set
        timeDwell = float(primeFlushAspirateInput_dwell.get())
        pumpControl.setMoveTimeOutPeriod(timeMove, timeDwell)
        pumpControl.setMoveType(movetype)


    def primeFlushDispenseControl(movetype = "Manual"):
        control.debugControl("=========== Prime/Flush Dispense =========== ")
        
        #Set defaults to zero
        volumeInput = 0
        speedInput = 0
        rampInput = 0
        
        #Make sure input is an integer
        try:
            volumeInput =   int(primeFlushDispenseInput_Volume.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Volume")
            primeFlushDispenseInput_Volume.delete(0, END)
            primeFlushDispenseInput_Volume.insert(0, pumpControl.defaultValuesForMC["Volume"])

        try:    
            speedInput =    int(primeFlushDispenseInput_Speed.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Speed")
            primeFlushDispenseInput_Speed.delete(0, END)
            primeFlushDispenseInput_Speed.insert(0, pumpControl.defaultValuesForMC["Speed"])
            
        try:    
            rampInput = int(primeFlushDispenseInput_ramp.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Ramp")
            primeFlushDispenseInput_ramp.delete(0, END)
            primeFlushDispenseInput_ramp.insert(0, pumpControl.defaultValuesForMC["Ramp"])
        
        try:
            dwellInput =    float(primeFlushDispenseInput_dwell.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an floating point value for: Dwell")
            primeFlushDispenseInput_dwell.delete(0, END)
            primeFlushDispenseInput_dwell.insert(0, pumpControl.defaultValuesForMC["Dwell"])
        
        #Update value based after checking range
        volumeInput = pumpControl.checkInputForRange("Volume", volumeInput)
        primeFlushDispenseInput_Volume.delete(0, END)
        primeFlushDispenseInput_Volume.insert(0, volumeInput)
        
        speedInput = pumpControl.checkInputForRange("Speed", speedInput)
        primeFlushDispenseInput_Speed.delete(0, END)
        primeFlushDispenseInput_Speed.insert(0, speedInput)
        
        rampInput = pumpControl.checkInputForRange("Ramp", rampInput)
        primeFlushDispenseInput_ramp.delete(0, END)
        primeFlushDispenseInput_ramp.insert(0, rampInput)
        
        dwellInput = pumpControl.checkInputForRange("Dwell", dwellInput)
        primeFlushDispenseInput_dwell.delete(0, END)
        primeFlushDispenseInput_dwell.insert(0, dwellInput)
        
        # Signal TT Robot Pump is Busy doing a move
        pumpControl.Dig_Out_0_Low_Busy()

        # Send Relative Move command in the Dispense Direction 
        # Use time of the move to configure the move time out period
        timeMove = pumpControl.MoveRel(volumeInput, speedInput, rampInput, MOVE_DIRECTION_DISPENSE)   

        # Set motor move time and add extra dwell (pause) time if set
        timeDwell = float(primeFlushDispenseInput_dwell.get())
        pumpControl.setMoveTimeOutPeriod(timeMove, timeDwell)
        pumpControl.setMoveType(movetype)

    def aspirateControl(movetype = "Manual"):
        control.debugControl("================= Aspirate ================= ")

        #Set defaults to zero
        volumeInput = 0
        speedInput = 0
        rampInput = 0
        
        #Make sure input is an integer
        try:
            volumeInput =   int(aspirateInput_Volume.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Volume")
            aspirateInput_Volume.delete(0, END)
            aspirateInput_Volume.insert(0, pumpControl.defaultValuesForMC["Volume"])

        try:    
            speedInput =    int(aspirateInput_Speed.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Speed")
            aspirateInput_Speed.delete(0, END)
            aspirateInput_Speed.insert(0, pumpControl.defaultValuesForMC["Speed"])
            
        try:    
            rampInput = int(aspirateInput_ramp.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Ramp")
            aspirateInput_ramp.delete(0, END)
            aspirateInput_ramp.insert(0, pumpControl.defaultValuesForMC["Ramp"])
        
        try:
            dwellInput =    float(aspirateInput_dwell.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an floating point value for: Dwell")
            aspirateInput_dwell.delete(0, END)
            aspirateInput_dwell.insert(0, pumpControl.defaultValuesForMC["Dwell"])
        
        #Update value based after checking range
        volumeInput = pumpControl.checkInputForRange("Volume", volumeInput)
        aspirateInput_Volume.delete(0, END)
        aspirateInput_Volume.insert(0, volumeInput)
        
        speedInput = pumpControl.checkInputForRange("Speed", speedInput)
        aspirateInput_Speed.delete(0, END)
        aspirateInput_Speed.insert(0, speedInput)
        
        rampInput = pumpControl.checkInputForRange("Ramp", rampInput)
        aspirateInput_ramp.delete(0, END)
        aspirateInput_ramp.insert(0, rampInput)
        
        dwellInput = pumpControl.checkInputForRange("Dwell", dwellInput)
        aspirateInput_dwell.delete(0, END)
        aspirateInput_dwell.insert(0, dwellInput)
        
        
        # Signal TT Robot Pump is Busy doing a move
        pumpControl.Dig_Out_0_Low_Busy()

        # Move in the Aspirate Direction
        timeMove = pumpControl.MoveRel(volumeInput, speedInput, rampInput, MOVE_DIRECTION_ASPIRATE)

        # Set motor move time and add extra dwell (pause) time if set
        timeDwell = float(aspirateInput_dwell.get())
        pumpControl.setMoveTimeOutPeriod(timeMove, timeDwell)
        pumpControl.setMoveType(movetype)


    def dispenseControl(movetype = "Manual"):
        control.debugControl("================= Dispense ================= ")

       
        #Set defaults to zero
        volumeInput = 0
        speedInput = 0
        rampInput = 0

        #Make sure input is an integer
        try:
            volumeInput =   int(dispenseInput_Volume.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Volume")
            dispenseInput_Volume.delete(0, END)
            dispenseInput_Volume.insert(0, pumpControl.defaultValuesForMC["Volume"])

        try:    
            speedInput =    int(dispenseInput_Speed.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Speed")
            dispenseInput_Speed.delete(0, END)
            dispenseInput_Speed.insert(0, pumpControl.defaultValuesForMC["Speed"])
            
        try:    
            rampInput = int(dispenseInput_ramp.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Ramp")
            dispenseInput_ramp.delete(0, END)
            dispenseInput_ramp.insert(0, pumpControl.defaultValuesForMC["Ramp"])
        
        try:
            dwellInput =    float(dispenseInput_dwell.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an floating point value for: Dwell")
            dispenseInput_dwell.delete(0, END)
            dispenseInput_dwell.insert(0, pumpControl.defaultValuesForMC["Dwell"])
        
        #Update value based after checking range
        volumeInput = pumpControl.checkInputForRange("Volume", volumeInput)
        dispenseInput_Volume.delete(0, END)
        dispenseInput_Volume.insert(0, volumeInput)
        
        speedInput = pumpControl.checkInputForRange("Speed", speedInput)
        dispenseInput_Speed.delete(0, END)
        dispenseInput_Speed.insert(0, speedInput)
        
        rampInput = pumpControl.checkInputForRange("Ramp", rampInput)
        dispenseInput_ramp.delete(0, END)
        dispenseInput_ramp.insert(0, rampInput)
        
        dwellInput = pumpControl.checkInputForRange("Dwell", dwellInput)
        dispenseInput_dwell.delete(0, END)
        dispenseInput_dwell.insert(0, dwellInput)
                        
        # Signal TT Robot Pump is Busy doing a move
        pumpControl.Dig_Out_0_Low_Busy()

        # Move in the Dispense Direction
        timeMove = pumpControl.MoveRel(volumeInput, speedInput, rampInput, MOVE_DIRECTION_DISPENSE)   

        # Set motor move time and add extra dwell (pause) time if set
        timeDwell = float(dispenseInput_dwell.get())
        pumpControl.setMoveTimeOutPeriod(timeMove, timeDwell)
        pumpControl.setMoveType(movetype)

     
    def mixRinseControl(movetype = "Manual"):
        control.debugControl("=============== Rinse / Wash =============== ")

       
        # Mixing or Rinsing consists of one aspirate direction move, followed by a dispense direction move.
        # This command maintains the last move, so that the TT Robot can reposition the pipette probe, 
        # in between aspirate and dispense moves.  This allows for mixing with different Z height positions.

        #Set defaults to zero
        volumeInput = 0
        speedInput = 0
        rampInput = 0

        #Make sure input is an integer
        try:
            volumeInput =   int(mixRinseInput_Volume.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Volume")
            mixRinseInput_Volume.delete(0, END)
            mixRinseInput_Volume.insert(0, pumpControl.defaultValuesForMC["Volume"])

        try:    
            speedInput =    int(mixRinseInput_Speed.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Speed")
            mixRinseInput_Speed.delete(0, END)
            mixRinseInput_Speed.insert(0, pumpControl.defaultValuesForMC["Speed"])
            
        try:    
            rampInput = int(mixRinseInput_ramp.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for: Ramp")
            mixRinseInput_ramp.delete(0, END)
            mixRinseInput_ramp.insert(0, pumpControl.defaultValuesForMC["Ramp"])
        
        try:
            dwellInput =    float(mixRinseInput_dwell.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an floating point value for: Dwell")
            mixRinseInput_dwell.delete(0, END)
            mixRinseInput_dwell.insert(0, pumpControl.defaultValuesForMC["Dwell"])
        
        #Update value based after checking range
        volumeInput = pumpControl.checkInputForRange("Volume", volumeInput)
        mixRinseInput_Volume.delete(0, END)
        mixRinseInput_Volume.insert(0, volumeInput)
        
        speedInput = pumpControl.checkInputForRange("Speed", speedInput)
        mixRinseInput_Speed.delete(0, END)
        mixRinseInput_Speed.insert(0, speedInput)
        
        rampInput = pumpControl.checkInputForRange("Ramp", rampInput)
        mixRinseInput_ramp.delete(0, END)
        mixRinseInput_ramp.insert(0, rampInput)
        
        dwellInput = pumpControl.checkInputForRange("Dwell", dwellInput)
        mixRinseInput_dwell.delete(0, END)
        mixRinseInput_dwell.insert(0, dwellInput)

        # Signal TT Robot Pump is Busy doing a move
        pumpControl.Dig_Out_0_Low_Busy()

        # Determine the move direction each time the Mix Rinse function is called.
        # Program initializes to Dispense direction.
        # Then this logic will do an Aspirate direction move first, then Dispense, and repeat
        if (pumpControl.lastDirectionMixRinse == MOVE_DIRECTION_DISPENSE):
            pumpControl.lastDirectionMixRinse = MOVE_DIRECTION_ASPIRATE
        else:
            pumpControl.lastDirectionMixRinse = MOVE_DIRECTION_DISPENSE

        timeMove = pumpControl.MoveRel(volumeInput, speedInput, rampInput,
                            pumpControl.lastDirectionMixRinse)    

        # Set motor move time and add extra dwell (pause) time if set
        timeDwell = float(mixRinseInput_dwell.get())
        pumpControl.setMoveTimeOutPeriod(timeMove, timeDwell)
        pumpControl.setMoveType(movetype)
        
    def airgapControl(movetype = "Manual"):
        control.debugControl("================= Air Gap ================== ")

        #Set defaults to zero
        volumeInput = 0
        speedInput = 0
        rampInput = 0
        
        #Make sure input is an integer
        try:
            volumeInput =   int(airgapInput_Volume.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for Volume")
            airgapInput_Volume.delete(0, END)
            airgapInput_Volume.insert(0, pumpControl.defaultValuesForMC["Volume"])

        try:    
            speedInput =    int(airgapInput_Speed.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for Speed")
            airgapInput_Speed.delete(0, END)
            airgapInput_Speed.insert(0, pumpControl.defaultValuesForMC["Speed"])
            
        try:    
            rampInput = int(airgapInput_ramp.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an integer value for Ramp")
            airgapInput_ramp.delete(0, END)
            airgapInput_ramp.insert(0, pumpControl.defaultValuesForMC["Ramp"])
        
        try:
            dwellInput =    float(airgapInput_dwell.get())
        except ValueError:
            tk.messagebox.showerror(title = "Error", message = "Please enter an floating point value for Dwell")
            airgapInput_dwell.delete(0, END)
            airgapInput_dwell.insert(0, pumpControl.defaultValuesForMC["Dwell"])
        
        #Update value based after checking range
        volumeInput = pumpControl.checkInputForRange("Volume", volumeInput)
        airgapInput_Volume.delete(0, END)
        airgapInput_Volume.insert(0, volumeInput)
        
        speedInput = pumpControl.checkInputForRange("Speed", speedInput)
        airgapInput_Speed.delete(0, END)
        airgapInput_Speed.insert(0, speedInput)
        
        rampInput = pumpControl.checkInputForRange("Ramp", rampInput)
        airgapInput_ramp.delete(0, END)
        airgapInput_ramp.insert(0, rampInput)
        
        dwellInput = pumpControl.checkInputForRange("Dwell", dwellInput)
        airgapInput_dwell.delete(0, END)
        airgapInput_dwell.insert(0, dwellInput)

        # Signal TT Robot Pump is Busy doing a move
        pumpControl.Dig_Out_0_Low_Busy()

        # Move in the Aspirate Direction
        timeMove = pumpControl.MoveRel(volumeInput, speedInput, rampInput, MOVE_DIRECTION_ASPIRATE)   

        # Set motor move time and add extra dwell (pause) time if set
        timeDwell = float(airgapInput_dwell.get())
        pumpControl.setMoveTimeOutPeriod(timeMove, timeDwell)
        pumpControl.setMoveType(movetype)

    # Serial Tx and Rx Done Here
    # We send the Nive (9) byte - binary motor command to the TMCM-1140 motion controller
    # It responds with a (9) byte reply, so we block and make sure we get it right away
    #
    def transmitDataNow(dataset):

        # Tx: Send command to TMCM controller
        pumpControl.i_ser.write(pumpControl.checksumCalculator(dataset))

        # flush the serial
        pumpControl.i_ser.flush()

        # Loop until input buffer has the full number of bytes for reply read
        # Testing shows this loop will execute less than 10 times typically
        #
        no_infinite_loop = 0
        numberOfBytesReady = comPortControl.i_serialObject.in_waiting
        while numberOfBytesReady < sizeOfEachMessageFromTMCL:
            numberOfBytesReady = comPortControl.i_serialObject.in_waiting
            no_infinite_loop += 1
            if (no_infinite_loop >1000): break
        
        # Rx: Read reply to TMCM command
        replyMsg = comPortControl.i_serialObject.read(sizeOfEachMessageFromTMCL)

        # Check reply msg for success
        pumpControl.checkReturnCode(replyMsg[2])    
                                 
        #control.debugControl("transmitDataNow Rx:")
        if ( (SHOW_HEX == 1) or (VERBOSE == 1) ): 
            if ( (pumpControl.lastState != 0) or (VERBOSE == 1)):
                control.debugControl(reply_Msg, "hex") # the received message

        return replyMsg

    def checkReturnCode(rtnCode):
        if (rtnCode != SUCCESS_CODE and (pumpControl.error_check_enabled)):
            tk.messagebox.showwarning(title = "Command Error", message = "Last command failed: \nError Code is {0}".format(rtnCode) )    

    def checksumCalculator(arrayDataSet):
        rollingSum = 0
        for index in range(0,len(arrayDataSet)):
            rollingSum += arrayDataSet[index]

        b1 = rollingSum & 0xff
        arrayDataSet.append(b1) 

        if (pumpControl.show_cmds == True): 
            control.debugControl(arrayDataSet, "hex")            

        return arrayDataSet

    def checksumCalculator_CheckOnly(arrayDataSet, sizeToCheck):
        rollingSum = 0
        for index in range(0,sizeToCheck):
            rollingSum += arrayDataSet[index]

        b1 = rollingSum & 0xff
        return b1


    def debugControlOfBusyIO():
        pumpControl.Check_Left_Limit_Switch_Status()

        if (pumpControl.busyToggleStatus == 0):
            control.debugControl("Toggling busy pin low busy")
            pumpControl.busyToggleStatus = 1
            pumpControl.Dig_Out_0_Low_Busy()

        else:
            control.debugControl("Toggling busy pin high Not busy")
            pumpControl.busyToggleStatus = 0
            pumpControl.Dig_Out_0_High_Not_Busy()


 

#############################################################################################
                     ######                          #####                                           
 ####   ####  #    # #     #  ####  #####  #####    #     #  ####  #    # ##### #####   ####  #      
#    # #    # ##  ## #     # #    # #    #   #      #       #    # ##   #   #   #    # #    # #      
#      #    # # ## # ######  #    # #    #   #      #       #    # # #  #   #   #    # #    # #      
#      #    # #    # #       #    # #####    #      #       #    # #  # #   #   #####  #    # #      
#    # #    # #    # #       #    # #   #    #      #     # #    # #   ##   #   #   #  #    # #      
 ####   ####  #    # #        ####  #    #   #       #####   ####  #    #   #   #    #  ####  ###### 

class comPortControl:

    #Serial Vars
    i_baudRate = 9600
    i_parityBits = 0
    i_timeOut = 1

    i_comPort = ''
    i_comPortPrevious = ''

    i_serialObject = 0

    receivedMsgQueue = RingBuffer()

    #Define all COM Ports
    def define_ports():
        
        #Lists COM ports
        ports = serial.tools.list_ports.comports()
        
        return ports

    def setComPort(stringVal):

        if 'USB' in stringVal: 
            splitPort = stringVal.split(' ')
            comPortControl.i_comPort = (splitPort[0])

    def setPreviousComPort(number):
        comPortControl.i_comPortPrevious = number

    def getComPort():
        return comPortControl.i_comPort, comPortControl.i_comPortPrevious

    def checkIfComPortIsInUse():
        if (comPortControl.i_serialObject != 0):
            return True
        else:
            return False

    def manageCommunicationPorts():
        # Set Com Port, else warn user and break program
        comPortControl.setComPort(dropdownComms.get())
        t_comPort, t_comportPrevious = comPortControl.getComPort()
        if t_comPort != "":
            # comport is set. check if it is different than before
            if (t_comPort != t_comportPrevious and t_comportPrevious == ""):
                # change from before, and the previous was zero. so just open the new port
                try: 
                    comPortControl.i_serialObject = serial.Serial(port = t_comPort, baudrate = comPortControl.i_baudRate, bytesize = 8, 
                                                                timeout = comPortControl.i_timeOut, stopbits = serial.STOPBITS_ONE)
                except:
                    messagebox.showinfo(title=None, message="COM Port Not Available")
                else:
                    control.debugControl("")
                    control.debugControl("Selected COM Port: " + str(t_comPort))
                    control.debugControl("COM Port selection successful")
                    control.debugControl("")

                # set the old port so we dont go back in
                comPortControl.setPreviousComPort(t_comPort)

                # now tell pump control the object for user in writing
                pumpControl.setPySerialObject(comPortControl.i_serialObject)
            elif (t_comPort != t_comportPrevious and t_comportPrevious != 0):
                # change from before, and the previous was not zero. close the old then open a new
                comPortControl.i_serialObject.close()
                try: 
                    comPortControl.i_serialObject = serial.Serial(port = t_comPort, baudrate = comPortControl.i_baudRate, bytesize = 8, 
                                                                timeout = comPortControl.i_timeOut, stopbits = serial.STOPBITS_ONE)
                except:
                    messagebox.showinfo(title=None, message="COM Port Not Available")
                else:    
                    control.debugControl("")
                    control.debugControl("Selected COM Port:", t_comPort)
                    control.debugControl("COM Port selection successful")
                    control.debugControl("")

                # set the old port so we dont go back in
                comPortControl.setPreviousComPort(t_comPort)

                # now tell pump control the object for user in writing
                pumpControl.setPySerialObject(comPortControl.i_serialObject)


#############################################################################################
######                                 #     #                               
#     # ###### #####  #    #  ####     #  #  # # #    # #####   ####  #    # 
#     # #      #    # #    # #    #    #  #  # # ##   # #    # #    # #    # 
#     # #####  #####  #    # #         #  #  # # # #  # #    # #    # #    # 
#     # #      #    # #    # #  ###    #  #  # # #  # # #    # #    # # ## # 
#     # #      #    # #    # #    #    #  #  # # #   ## #    # #    # ##  ## 
######  ###### #####   ####   ####      ## ##  # #    # #####   ####  #    # 
                                                                          
#######                                                                      
   #    #    # #####  ######   ##   #####                                    
   #    #    # #    # #       #  #  #    #                                   
   #    ###### #    # #####  #    # #    #                                   
   #    #    # #####  #      ###### #    #                                   
   #    #    # #   #  #      #    # #    #                                   
   #    #    # #    # ###### #    # #####         

class IO_Query(threading.Thread):
    # Class to check the current IO status every second

    def __init__(self):
        super().__init__()
        self._stop_event = threading.Event()

    def run(self):
        previous = -1
        while not self._stop_event.is_set():
            time.sleep(1)

    def stop(self):
        self._stop_event.set()

class QueueHandler(logging.Handler):
    # Class to send logging records to a queue
    # The textWindow class polls this queue to display records in a ScrolledText widget

    def __init__(self, log_queue):
        super().__init__()
        self.log_queue = log_queue

    def emit(self, record):
        self.log_queue.put(record)

class textWindow:
    """Poll messages from a logging queue and display them in a scrolled text widget"""

    def __init__(self, frame):
        self.frame = frame
        # Create a ScrolledText wdiget
        self.scrolled_text = ScrolledText(frame, state='disabled', height=TEXT_WINDOW_HEIGHT, width = 95)
        self.scrolled_text.grid(row=0, column=0, sticky=(N, S, W, E))
        self.scrolled_text.configure(font='TkFixedFont')
        self.scrolled_text.tag_config('INFO', foreground='black')
        self.scrolled_text.tag_config('DEBUG', foreground='orange')
        # Create a logging handler using a queue
        self.log_queue = queue.Queue()
        self.queue_handler = QueueHandler(self.log_queue)
        formatter = logging.Formatter('%(asctime)s: %(message)s','%m/%d/%Y %I:%M:%S %p')
        self.queue_handler.setFormatter(formatter)
        logger.addHandler(self.queue_handler)
        # Start polling messages from the queue
        self.frame.after(100, self.poll_log_queue)

    def display(self, record):
        msg = self.queue_handler.format(record)
        self.scrolled_text.configure(state='normal')
        self.scrolled_text.insert(tk.END, msg + '\n', record.levelname)
        self.scrolled_text.configure(state='disabled')
        # Autoscroll to the bottom
        self.scrolled_text.yview(tk.END)

    def poll_log_queue(self):
        # Check every 100ms if there is a new message in the queue to display
        while True:
            try:
                record = self.log_queue.get(block=False)
            except queue.Empty:
                break
            else:
                self.display(record)
        self.frame.after(100, self.poll_log_queue)

class consoleConfiguration:

    def __init__(self, root):
        self.root = root
        self.clock = IO_Query()
        self.clock.start()

    def quit(self, *args):
        self.clock.stop()
        self.root.destroy()

#############################################################################################
  #####  #     # ###     #####                             
 #     # #     #  #     #     # ###### ##### #    # #####  
 #       #     #  #     #       #        #   #    # #    # 
 #  #### #     #  #      #####  #####    #   #    # #    # 
 #     # #     #  #           # #        #   #    # #####  
 #     # #     #  #     #     # #        #   #    # #      
  #####   #####  ###     #####  ######   #    ####  #  

#
# Prime / Flush Aspirate Callbacks for Value Changes
#
def pFA_volume_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH ASPIRATE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH ASPIRATE")

def pFA_speed_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH ASPIRATE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH ASPIRATE")

def pFA_ramp_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH ASPIRATE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH ASPIRATE")

# Note presently dwells are not used in the setSpeed and SetRampfunctions
# so not really needed to check here.  This callback included for symmetry
# and for future use, if needed.  This comment applies to all six
# functions.
def pFA_dwell_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH ASPIRATE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH ASPIRATE")

#
# Prime / Flush Dispense Callbacks for Value Changes
#
def pFD_volume_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH DISPENSE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH DISPENSE")

def pFD_speed_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH DISPENSE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH DISPENSE")

def pFD_ramp_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH DISPENSE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH DISPENSE")

def pFD_dwell_changed(*args):
    pumpControl.setSpeedString_inuLPerS("PRIME / FLUSH DISPENSE")
    pumpControl.setRamp_inPercentage("PRIME / FLUSH DISPENSE")
#
# Aspirate Callbacks for Value Changes
#
def aSP_volume_changed(*args):
    pumpControl.setSpeedString_inuLPerS("ASPIRATE")
    pumpControl.setRamp_inPercentage("ASPIRATE")

def aSP_speed_changed(*args):
    pumpControl.setSpeedString_inuLPerS("ASPIRATE")
    pumpControl.setRamp_inPercentage("ASPIRATE")

def aSP_ramp_changed(*args):
    pumpControl.setSpeedString_inuLPerS("ASPIRATE")
    pumpControl.setRamp_inPercentage("ASPIRATE")

def aSP_dwell_changed(*args):
    pumpControl.setSpeedString_inuLPerS("ASPIRATE")
    pumpControl.setRamp_inPercentage("ASPIRATE")
#
# Dispense Callbacks for Value Changes
#
def dSP_volume_changed(*args):
    pumpControl.setSpeedString_inuLPerS("DISPENSE")
    pumpControl.setRamp_inPercentage("DISPENSE")

def dSP_speed_changed(*args):
    pumpControl.setSpeedString_inuLPerS("DISPENSE")
    pumpControl.setRamp_inPercentage("DISPENSE")

def dSP_ramp_changed(*args):
    pumpControl.setSpeedString_inuLPerS("DISPENSE")
    pumpControl.setRamp_inPercentage("DISPENSE")

def dSP_dwell_changed(*args):
    pumpControl.setSpeedString_inuLPerS("DISPENSE")
    pumpControl.setRamp_inPercentage("DISPENSE")   
#
# Mix / Rinse Callbacks for Value Changes
#
def mR_volume_changed(*args):
    pumpControl.setSpeedString_inuLPerS("RINSE / WASH")
    pumpControl.setRamp_inPercentage("RINSE / WASH")

def mR_speed_changed(*args):
    pumpControl.setSpeedString_inuLPerS("RINSE / WASH")
    pumpControl.setRamp_inPercentage("RINSE / WASH")

def mR_ramp_changed(*args):
    pumpControl.setSpeedString_inuLPerS("RINSE / WASH")
    pumpControl.setRamp_inPercentage("RINSE / WASH")

def mR_dwell_changed(*args):
    pumpControl.setSpeedString_inuLPerS("RINSE / WASH")
    pumpControl.setRamp_inPercentage("RINSE / WASH")
#
# Air Gap Callbacks for Value Changes
#
def aG_volume_changed(*args):
    pumpControl.setSpeedString_inuLPerS("AIR GAP")
    pumpControl.setRamp_inPercentage("AIR GAP")

def aG_speed_changed(*args):
    pumpControl.setSpeedString_inuLPerS("AIR GAP")
    pumpControl.setRamp_inPercentage("AIR GAP")

def aG_ramp_changed(*args):
    pumpControl.setSpeedString_inuLPerS("AIR GAP")
    pumpControl.setRamp_inPercentage("AIR GAP")

def aG_dwell_changed(*args):
    pumpControl.setSpeedString_inuLPerS("AIR GAP")
    pumpControl.setRamp_inPercentage("AIR GAP")




# GUI setup and configuration settings
if __name__ == '__main__':
    try:
        logging.basicConfig(level=logging.DEBUG)

        # Call above functions to define Com Port
        # we will list out all comm ports for the user to pick it
        availablePorts = comPortControl.define_ports()
        if (len(availablePorts) == 0):
            availablePorts.append("No ports available, restart GUI")

        # GUI general setup
        root = tk.Tk()

        width_x_height_string = str(APP_WIDTH) + "x" + str(APP_HEIGHT)
        root.geometry(width_x_height_string)
        root.configure(background="#ABABAB")
        versionString = str(codeVersion_Major) + "." + str(codeVersion_Minor) + "." + str(codeVersion_Build)
        root.title("Benchtop Pipetter System: V" + versionString)

        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        # Create the panes and frames
        vertical_pane = ttk.PanedWindow(root, orient=VERTICAL)
        vertical_pane.grid(row=0, column=0, sticky="nsew")
        horizontal_pane = ttk.PanedWindow(vertical_pane, orient=HORIZONTAL)
        vertical_pane.add(horizontal_pane)
        console_frame = ttk.Labelframe(horizontal_pane, text="Command Status")
        console_frame.columnconfigure(0, weight=1)
        console_frame.rowconfigure(10, weight=1)
        horizontal_pane.add(console_frame, weight=1)

        # Initialize frames
        console_frame.place(rely=1.0, relx=1.0, x=0, y=0, anchor=SE)
        textWindow(console_frame)

        # dropdown for com ports available
        # set label for comm dropdown first
        comPortFrame = ttk.Labelframe(horizontal_pane, text="Select COM Port")
        comPortFrame.columnconfigure(0, weight=1)
        comPortFrame.rowconfigure(10, weight=1)
        horizontal_pane.add(comPortFrame, weight=1)
        comPortFrame.place(x=0, y=0)
        dropdownComms = tk.StringVar(root)
        if (len(availablePorts) == 1):
            dropdownComms.set(availablePorts[0])
        else:
            dropdownComms.set("Select a COM Port")
        dropdownComms2 = tk.OptionMenu(comPortFrame, dropdownComms, *availablePorts)
        dropdownComms2.config(width=35, font=('Helvetica', 8))
        dropdownComms2.grid(row=1, column=1, rowspan = 4, pady = 10)

        consoleConfiguration = consoleConfiguration(root)

        wholeFrameTracking_x = 0
        wholeFrameTracking_y = 0
        padding_y = 0
        framePosition = 0
        buttonWidth = 10

        # home button for commands
        wholeFrameTracking_y += 0
        homeFrame = ttk.Labelframe(horizontal_pane, text="Go Home")
        homeFrame.columnconfigure(0, weight=1)
        homeFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(homeFrame, weight=1)
        homeFrame.place(x=300, y=wholeFrameTracking_y)
        homeTrigger = tk.Button(homeFrame, width=buttonWidth, text ="Home", command = pumpControl.HomeMotor)
        LSCheckboxvar = tk.IntVar()
        LSCheckbox = tk.Checkbutton(homeFrame, text='L/S', state=DISABLED, variable = LSCheckboxvar, onvalue = 1, offvalue = 0)
        homeTrigger.grid(row=0, column=1, rowspan = 4, pady = 10)
        LSCheckbox.grid(row=0, column=30, rowspan = 4, pady = 10)

        # checkboxes for IO checking
        # update function for the output control

        ioCheckFrame = ttk.Labelframe(horizontal_pane, text="IO Status and Control")
        ioCheckFrame.columnconfigure(0, weight=1)
        ioCheckFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(ioCheckFrame, weight=1)
        ioCheckFrame.place(x=500, y=wholeFrameTracking_y)
        io1StatusBoxvar = tk.IntVar()
        io1StatusBox = tk.Checkbutton(ioCheckFrame, text='I1', state=DISABLED, variable = io1StatusBoxvar, onvalue = 1, offvalue = 0)
        io2StatusBoxvar = tk.IntVar()
        io2StatusBox = tk.Checkbutton(ioCheckFrame, text='I2', state=DISABLED, variable = io2StatusBoxvar, onvalue = 1, offvalue = 0)
        io3StatusBoxvar = tk.IntVar()
        io3StatusBox = tk.Checkbutton(ioCheckFrame, text='I3', state=DISABLED, variable = io3StatusBoxvar, onvalue = 1, offvalue = 0)
        BusyStatusBoxvar = tk.IntVar()
        BusyStatusBox = tk.Checkbutton(ioCheckFrame, text='Busy', state=DISABLED, variable = BusyStatusBoxvar, onvalue = 1, offvalue = 0, command = pumpControl.setOutputIO)
        io1StatusBox.grid(row=0, column=60, rowspan = 4, pady = 10) #TODO - change order
        io2StatusBox.grid(row=0, column=30, rowspan = 4, pady = 10)
        io3StatusBox.grid(row=0, column=0, rowspan = 4, pady = 10)
        BusyStatusBox.grid(row=0, column=90, rowspan = 4, pady = 10)

        # stop the motor
        motorStopFrame = ttk.Labelframe(horizontal_pane)
        motorStopFrame.columnconfigure(0, weight=1)
        motorStopFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(motorStopFrame, weight=1)
        motorStopFrame.place(x=800, y=0)
        stopTrigger = tk.Button(motorStopFrame, width=buttonWidth, font=('Helvetica', 20), text ="STOP", command = pumpControl.stopMotor)
        stopTrigger.grid(row=3, column=1, rowspan = 4, pady = 1)

        ################################################################################
        # setup the Prime / Flush Aspirate frame
        ################################################################################
        wholeFrameTracking_y += 100
        primeFlushAspirate = ttk.Labelframe(horizontal_pane, text = "PRIME / FLUSH ASPIRATE", labelanchor = 'n')
        primeFlushAspirate.columnconfigure(0, weight=1)
        primeFlushAspirate.rowconfigure(0, weight=1)
        horizontal_pane.add(primeFlushAspirate, weight=1)
        primeFlushAspirate.place(x=0, y=wholeFrameTracking_y)

        # Define strings and then use the trace command to link the callback function 
        # which will get called anytime the text entry field changes

        pFA_volume=tk.StringVar(value = INIT_PF_ASP_VOL)
        pFA_volume.trace("w", pFA_volume_changed)

        pFA_speed=tk.StringVar(value = INIT_PF_ASP_SPEED)
        pFA_speed.trace("w", pFA_speed_changed)

        pFA_ramp=tk.StringVar(value = INIT_PF_ASP_RAMP)
        pFA_ramp.trace("w", pFA_ramp_changed)

        pFA_dwell=tk.StringVar(value = INIT_PF_ASP_DWELL)
        pFA_dwell.trace("w", pFA_dwell_changed)

        primeFlushAspirateLabel_Volume = tk.Label(primeFlushAspirate, text = "Volume")
        primeFlushAspirateLabel_Volume.grid(row=1, column=0, rowspan = 4, pady = 10)
        primeFlushAspirateInput_Volume = tk.Entry(primeFlushAspirate, width=8, borderwidth=5, textvariable = pFA_volume)
        
        primeFlushAspirateInput_Volume.grid(row=1, column=50, rowspan = 4, pady = 10)
        primeFlushAspirateLabel_uL = tk.Label(primeFlushAspirate, text = "uL")
        primeFlushAspirateLabel_uL.grid(row=1, column=100, rowspan = 4, pady = 10)
        
        primeFlushAspirateLabel_Speed = tk.Label(primeFlushAspirate, text = "Speed")
        primeFlushAspirateLabel_Speed.grid(row=10, column=0, rowspan = 4, pady = 10)
        primeFlushAspirateInput_Speed = tk.Entry(primeFlushAspirate, width=8, borderwidth=5, textvariable = pFA_speed)
        primeFlushAspirateInput_Speed.grid(row=10, column=50, rowspan = 4, pady = 10)
        primeFlushAspirateLabel_SpeedUnit = tk.Label(primeFlushAspirate, text = "0...2047")
        primeFlushAspirateLabel_SpeedUnit.grid(row=10, column=100, rowspan = 4, pady = 10)

        primeFlushAspirateLabel_SpeedCalculated = tk.Label(primeFlushAspirate, text = "Speed")
        primeFlushAspirateLabel_SpeedCalculated.grid(row=20, column=0, rowspan = 4, pady = 10)
        primeFlushAspirateInput_SpeedCalculated = tk.Entry(primeFlushAspirate, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        primeFlushAspirateInput_SpeedCalculated.grid(row=20, column=50, rowspan = 4, pady = 10)
        primeFlushAspirateLabel_SpeedCalculatedUnit = tk.Label(primeFlushAspirate, text = "uL/s")
        primeFlushAspirateLabel_SpeedCalculatedUnit.grid(row=20, column=100, rowspan = 4, pady = 10)

        primeFlushAspirateLabel_ramp = tk.Label(primeFlushAspirate, text = "Ramp")
        primeFlushAspirateLabel_ramp.grid(row=30, column=0, rowspan = 4, pady = 10)
        primeFlushAspirateInput_ramp = tk.Entry(primeFlushAspirate, width=8, borderwidth=5, textvariable = pFA_ramp)
        primeFlushAspirateInput_ramp.grid(row=30, column=50, rowspan = 4, pady = 10)
        primeFlushAspirateLabel_rampUnit = tk.Label(primeFlushAspirate, text = "0...2047")
        primeFlushAspirateLabel_rampUnit.grid(row=30, column=100, rowspan = 4, pady = 10)

        primeFlushAspirateLabel_rampCalculated = tk.Label(primeFlushAspirate, text = "Ramp")
        primeFlushAspirateLabel_rampCalculated.grid(row=40, column=0, rowspan = 4, pady = 10)
        primeFlushAspirateInput_rampCalculated = tk.Entry(primeFlushAspirate, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        primeFlushAspirateInput_rampCalculated.grid(row=40, column=50, rowspan = 4, pady = 10)
        primeFlushAspirateLabel_rampCalculatedUnit = tk.Label(primeFlushAspirate, text = "%")
        primeFlushAspirateLabel_rampCalculatedUnit.grid(row=40, column=100, rowspan = 4, pady = 10)

        primeFlushAspirateLabel_dwell = tk.Label(primeFlushAspirate, text = "Dwell")
        primeFlushAspirateLabel_dwell.grid(row=50, column=0, rowspan = 4, pady = 10)
        primeFlushAspirateInput_dwell = tk.Entry(primeFlushAspirate, width=8, borderwidth=5, textvariable = pFA_dwell)
        primeFlushAspirateInput_dwell.grid(row=50, column=50, rowspan = 4, pady = 10)
        primeFlushAspirateLabel_dwellUnit = tk.Label(primeFlushAspirate, text = "s")
        primeFlushAspirateLabel_dwellUnit.grid(row=50, column=100, rowspan = 4, pady = 10)

        primeFlushAspirateTrigger = tk.Button(primeFlushAspirate, width=buttonWidth, text ="Manual", command = pumpControl.primeFlushAspirateControl)
        primeFlushAspirateTrigger.grid(row=60, column=50, pady = 10)

        primeFlushAspiratetemp = tk.Label(primeFlushAspirate, text = "AUTO INPUTS: 001")
        primeFlushAspiratetemp.grid(row=70, column=50, rowspan = 4, pady = 2)





        ################################################################################
        # setup the Prime / Flush Dispense frame
        ################################################################################
        wholeFrameTracking_x += 200
        primeFlushDispense = ttk.Labelframe(horizontal_pane, text = "PRIME / FLUSH DISPENSE", labelanchor = 'n')
        primeFlushDispense.columnconfigure(0, weight=1)
        primeFlushDispense.rowconfigure(0, weight=1)
        horizontal_pane.add(primeFlushDispense, weight=1)
        primeFlushDispense.place(x=wholeFrameTracking_x, y=wholeFrameTracking_y)

        # Define strings and then use the trace command to link the callback function 
        # which will get called anytime the text entry field changes

        pFD_volume=tk.StringVar(value = INIT_PF_DIS_VOL)
        pFD_volume.trace("w", pFD_volume_changed)

        pFD_speed=tk.StringVar(value = INIT_PF_DIS_SPEED)
        pFD_speed.trace("w", pFD_speed_changed)

        pFD_ramp=tk.StringVar(value = INIT_PF_DIS_RAMP)
        pFD_ramp.trace("w", pFD_ramp_changed)

        pFD_dwell=tk.StringVar(value = INIT_PF_DIS_DWELL)
        pFD_dwell.trace("w", pFD_dwell_changed)

        primeFlushDispenseLabel_Volume = tk.Label(primeFlushDispense, text = "Volume")
        primeFlushDispenseLabel_Volume.grid(row=1, column=0, rowspan = 4, pady = 10)
        primeFlushDispenseInput_Volume = tk.Entry(primeFlushDispense, width=8, borderwidth=5, textvariable = pFD_volume)
        primeFlushDispenseInput_Volume.grid(row=1, column=50, rowspan = 4, pady = 10)
        primeFlushDispenseLabel_uL = tk.Label(primeFlushDispense, text = "uL")
        primeFlushDispenseLabel_uL.grid(row=1, column=100, rowspan = 4, pady = 10)

        primeFlushDispenseLabel_Speed = tk.Label(primeFlushDispense, text = "Speed")
        primeFlushDispenseLabel_Speed.grid(row=10, column=0, rowspan = 4, pady = 10)
        primeFlushDispenseInput_Speed = tk.Entry(primeFlushDispense, width=8, borderwidth=5, textvariable = pFD_speed)
        primeFlushDispenseInput_Speed.grid(row=10, column=50, rowspan = 4, pady = 10)
        primeFlushDispenseLabel_SpeedUnit = tk.Label(primeFlushDispense, text = "0...2047")
        primeFlushDispenseLabel_SpeedUnit.grid(row=10, column=100, rowspan = 4, pady = 10)

        primeFlushDispenseLabel_SpeedCalculated = tk.Label(primeFlushDispense, text = "Speed")
        primeFlushDispenseLabel_SpeedCalculated.grid(row=20, column=0, rowspan = 4, pady = 10)
        primeFlushDispenseInput_SpeedCalculated = tk.Entry(primeFlushDispense, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        primeFlushDispenseInput_SpeedCalculated.grid(row=20, column=50, rowspan = 4, pady = 10)
        primeFlushDispenseLabel_SpeedCalculatedUnit = tk.Label(primeFlushDispense, text = "uL/s")
        primeFlushDispenseLabel_SpeedCalculatedUnit.grid(row=20, column=100, rowspan = 4, pady = 10)

        primeFlushDispenseLabel_ramp = tk.Label(primeFlushDispense, text = "Ramp")
        primeFlushDispenseLabel_ramp.grid(row=30, column=0, rowspan = 4, pady = 10)
        primeFlushDispenseInput_ramp = tk.Entry(primeFlushDispense, width=8, borderwidth=5, textvariable = pFD_ramp)
        primeFlushDispenseInput_ramp.grid(row=30, column=50, rowspan = 4, pady = 10)
        primeFlushDispenseLabel_rampUnit = tk.Label(primeFlushDispense, text = "0...2047")
        primeFlushDispenseLabel_rampUnit.grid(row=30, column=100, rowspan = 4, pady = 10)

        primeFlushDispenseLabel_rampCalculated = tk.Label(primeFlushDispense, text = "Ramp")
        primeFlushDispenseLabel_rampCalculated.grid(row=40, column=0, rowspan = 4, pady = 10)
        primeFlushDispenseInput_rampCalculated = tk.Entry(primeFlushDispense, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        primeFlushDispenseInput_rampCalculated.grid(row=40, column=50, rowspan = 4, pady = 10)
        primeFlushDispenseLabel_rampCalculatedUnit = tk.Label(primeFlushDispense, text = "%")
        primeFlushDispenseLabel_rampCalculatedUnit.grid(row=40, column=100, rowspan = 4, pady = 10)

        primeFlushDispenseLabel_dwell = tk.Label(primeFlushDispense, text = "Dwell")
        primeFlushDispenseLabel_dwell.grid(row=50, column=0, rowspan = 4, pady = 10)
        primeFlushDispenseInput_dwell = tk.Entry(primeFlushDispense, width=8, borderwidth=5, textvariable = pFD_dwell)
        primeFlushDispenseInput_dwell.grid(row=50, column=50, rowspan = 4, pady = 10)
        primeFlushDispenseLabel_dwellUnit = tk.Label(primeFlushDispense, text = "s")
        primeFlushDispenseLabel_dwellUnit.grid(row=50, column=100, rowspan = 4, pady = 10)

        primeFlushTrigger = tk.Button(primeFlushDispense, width=buttonWidth, text ="Manual", command = pumpControl.primeFlushDispenseControl)
        primeFlushTrigger.grid(row=60, column=50, pady = 10)


        primeFlushDispensetemp = tk.Label(primeFlushDispense, text = "AUTO INPUTS: 010")
        primeFlushDispensetemp.grid(row=70, column=50, rowspan = 4, pady = 2)

        ################################################################################
        # setup the aspirate frame
        ################################################################################
        wholeFrameTracking_x += 200
        aspirateFrame = ttk.Labelframe(horizontal_pane, text = "ASPIRATE", labelanchor = 'n')
        aspirateFrame.columnconfigure(0, weight=1)
        aspirateFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(aspirateFrame, weight=1)
        aspirateFrame.place(x=wholeFrameTracking_x, y=wholeFrameTracking_y)

        # Define strings and then use the trace command to link the callback function 
        # which will get called anytime the text entry field changes

        aSP_volume=tk.StringVar(value = INIT_ASP_VOL)
        aSP_volume.trace("w", aSP_volume_changed)

        aSP_speed=tk.StringVar(value = INIT_ASP_SPEED)
        aSP_speed.trace("w", aSP_speed_changed)

        aSP_ramp=tk.StringVar(value = INIT_ASP_RAMP)
        aSP_ramp.trace("w", aSP_ramp_changed)

        aSP_dwell=tk.StringVar(value = INIT_ASP_DWELL)
        aSP_dwell.trace("w", aSP_dwell_changed)


        aspirateLabel_Volume = tk.Label(aspirateFrame, text = "Volume")
        aspirateLabel_Volume.grid(row=1, column=0, rowspan = 4, pady = 10)
        aspirateInput_Volume = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = aSP_volume)
        aspirateInput_Volume.grid(row=1, column=50, rowspan = 4, pady = 10)
        aspirateLabel_uL = tk.Label(aspirateFrame, text = "uL")
        aspirateLabel_uL.grid(row=1, column=100, rowspan = 4, pady = 10)

        aspirateLabel_Speed = tk.Label(aspirateFrame, text = "Speed")
        aspirateLabel_Speed.grid(row=10, column=0, rowspan = 4, pady = 10)
        aspirateInput_Speed = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = aSP_speed)
        aspirateInput_Speed.grid(row=10, column=50, rowspan = 4, pady = 10)
        aspirateLabel_SpeedUnit = tk.Label(aspirateFrame, text = "0...2047")
        aspirateLabel_SpeedUnit.grid(row=10, column=100, rowspan = 4, pady = 10)

        aspirateLabel_SpeedCalculated = tk.Label(aspirateFrame, text = "Speed")
        aspirateLabel_SpeedCalculated.grid(row=20, column=0, rowspan = 4, pady = 10)
        aspirateInput_SpeedCalculated = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        aspirateInput_SpeedCalculated = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        aspirateInput_SpeedCalculated.grid(row=20, column=50, rowspan = 4, pady = 10)
        aspirateLabel_SpeedCalculatedUnit = tk.Label(aspirateFrame, text = "uL/s")
        aspirateLabel_SpeedCalculatedUnit.grid(row=20, column=100, rowspan = 4, pady = 10)

        aspirateLabel_ramp = tk.Label(aspirateFrame, text = "Ramp")
        aspirateLabel_ramp.grid(row=30, column=0, rowspan = 4, pady = 10)
        aspirateInput_ramp = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = aSP_ramp)
        aspirateInput_ramp.grid(row=30, column=50, rowspan = 4, pady = 10)
        aspirateLabel_rampUnit = tk.Label(aspirateFrame, text = "0...2047")
        aspirateLabel_rampUnit.grid(row=30, column=100, rowspan = 4, pady = 10)

        aspirateLabel_rampCalculated = tk.Label(aspirateFrame, text = "Ramp")
        aspirateLabel_rampCalculated.grid(row=40, column=0, rowspan = 4, pady = 10)
        aspirateInput_rampCalculated = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        aspirateInput_rampCalculated.grid(row=40, column=50, rowspan = 4, pady = 10)
        aspirateLabel_rampCalculatedUnit = tk.Label(aspirateFrame, text = "%")
        aspirateLabel_rampCalculatedUnit.grid(row=40, column=100, rowspan = 4, pady = 10)

        aspirateLabel_dwell = tk.Label(aspirateFrame, text = "Dwell")
        aspirateLabel_dwell.grid(row=50, column=0, rowspan = 4, pady = 10)
        aspirateInput_dwell = tk.Entry(aspirateFrame, width=8, borderwidth=5, textvariable = aSP_dwell)
        aspirateInput_dwell.grid(row=50, column=50, rowspan = 4, pady = 10)
        aspirateLabel_dwellUnit = tk.Label(aspirateFrame, text = "s")
        aspirateLabel_dwellUnit.grid(row=50, column=100, rowspan = 4, pady = 10)

        aspirateTrigger = tk.Button(aspirateFrame, width=buttonWidth, text ="Manual", command = pumpControl.aspirateControl)
        aspirateTrigger.grid(row=60, column=50, pady = 10)

        aspiratetemp = tk.Label(aspirateFrame, text = "AUTO INPUTS: 011")
        aspiratetemp.grid(row=70, column=50, rowspan = 4, pady = 2)

        ################################################################################
        # setup the dispense frame
        ################################################################################
        wholeFrameTracking_x += 200
        dispenseFrame = ttk.Labelframe(horizontal_pane, text = "DISPENSE", labelanchor = 'n')
        dispenseFrame.columnconfigure(0, weight=1)
        dispenseFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(dispenseFrame, weight=1)
        dispenseFrame.place(x=wholeFrameTracking_x, y=wholeFrameTracking_y)

        # Define strings and then use the trace command to link the callback function 
        # which will get called anytime the text entry field changes

        dSP_volume=tk.StringVar(value = INIT_DIS_VOL)
        dSP_volume.trace("w", dSP_volume_changed)

        dSP_speed=tk.StringVar(value = INIT_DIS_SPEED)
        dSP_speed.trace("w", dSP_speed_changed)

        dSP_ramp=tk.StringVar(value = INIT_DIS_RAMP)
        dSP_ramp.trace("w", dSP_ramp_changed)

        dSP_dwell=tk.StringVar(value = INIT_DIS_DWELL)
        dSP_dwell.trace("w", dSP_dwell_changed)


        dispenseLabel_Volume = tk.Label(dispenseFrame, text = "Volume")
        dispenseLabel_Volume.grid(row=1, column=0, rowspan = 4, pady = 10)
        dispenseInput_Volume = tk.Entry(dispenseFrame, width=8, borderwidth=5, textvariable = dSP_volume)
        dispenseInput_Volume.grid(row=1, column=50, rowspan = 4, pady = 10)
        dispenseLabel_uL = tk.Label(dispenseFrame, text = "uL")
        dispenseLabel_uL.grid(row=1, column=100, rowspan = 4, pady = 10)

        dispenseLabel_Speed = tk.Label(dispenseFrame, text = "Speed")
        dispenseLabel_Speed.grid(row=10, column=0, rowspan = 4, pady = 10)
        dispenseInput_Speed = tk.Entry(dispenseFrame, width=8, borderwidth=5, textvariable = dSP_speed)
        dispenseInput_Speed.grid(row=10, column=50, rowspan = 4, pady = 10)
        dispenseLabel_SpeedUnit = tk.Label(dispenseFrame, text = "0...2047")
        dispenseLabel_SpeedUnit.grid(row=10, column=100, rowspan = 4, pady = 10)

        dispenseLabel_SpeedCalculated = tk.Label(dispenseFrame, text = "Speed")
        dispenseLabel_SpeedCalculated.grid(row=20, column=0, rowspan = 4, pady = 10)
        dispenseInput_SpeedCalculated = tk.Entry(dispenseFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        dispenseInput_SpeedCalculated.grid(row=20, column=50, rowspan = 4, pady = 10)
        dispenseLabel_SpeedCalculatedUnit = tk.Label(dispenseFrame, text = "uL/s")
        dispenseLabel_SpeedCalculatedUnit.grid(row=20, column=100, rowspan = 4, pady = 10)

        dispenseLabel_ramp = tk.Label(dispenseFrame, text = "Ramp")
        dispenseLabel_ramp.grid(row=30, column=0, rowspan = 4, pady = 10)
        dispenseInput_ramp = tk.Entry(dispenseFrame, width=8, borderwidth=5, textvariable = dSP_ramp)
        dispenseInput_ramp.grid(row=30, column=50, rowspan = 4, pady = 10)
        dispenseLabel_rampUnit = tk.Label(dispenseFrame, text = "0...2047")
        dispenseLabel_rampUnit.grid(row=30, column=100, rowspan = 4, pady = 10)

        dispenseLabel_rampCalculated = tk.Label(dispenseFrame, text = "Ramp")
        dispenseLabel_rampCalculated.grid(row=40, column=0, rowspan = 4, pady = 10)
        dispenseInput_rampCalculated = tk.Entry(dispenseFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        dispenseInput_rampCalculated.grid(row=40, column=50, rowspan = 4, pady = 10)
        dispenseLabel_rampCalculatedUnit = tk.Label(dispenseFrame, text = "%")
        dispenseLabel_rampCalculatedUnit.grid(row=40, column=100, rowspan = 4, pady = 10)

        dispenseLabel_dwell = tk.Label(dispenseFrame, text = "Dwell")
        dispenseLabel_dwell.grid(row=50, column=0, rowspan = 4, pady = 10)
        dispenseInput_dwell = tk.Entry(dispenseFrame, width=8, borderwidth=5, textvariable = dSP_dwell)
        dispenseInput_dwell.grid(row=50, column=50, rowspan = 4, pady = 10)
        dispenseLabel_dwellUnit = tk.Label(dispenseFrame, text = "s")
        dispenseLabel_dwellUnit.grid(row=50, column=100, rowspan = 4, pady = 10)


        dispenseTrigger = tk.Button(dispenseFrame, width=buttonWidth, text ="Manual", command = pumpControl.dispenseControl)
        dispenseTrigger.grid(row=60, column=50, pady = 10)

        dispensetemp = tk.Label(dispenseFrame, text = "AUTO INPUTS: 100")
        dispensetemp.grid(row=70, column=50, rowspan = 4, pady = 2)

        ################################################################################
        # setup the mix/rinse frame
        ################################################################################
        wholeFrameTracking_x += 200
        mixRinseFrame = ttk.Labelframe(horizontal_pane, text = "RINSE / WASH", labelanchor = 'n')
        mixRinseFrame.columnconfigure(0, weight=1)
        mixRinseFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(mixRinseFrame, weight=1)
        mixRinseFrame.place(x=wholeFrameTracking_x, y=wholeFrameTracking_y)

        # Define strings and then use the trace command to link the callback function 
        # which will get called anytime the text entry field changes

        mR_volume=tk.StringVar(value = INIT_MR_VOL)
        mR_volume.trace("w", mR_volume_changed)

        mR_speed=tk.StringVar(value = INIT_MR_SPEED)
        mR_speed.trace("w", mR_speed_changed)

        mR_ramp=tk.StringVar(value = INIT_MR_RAMP)
        mR_ramp.trace("w", mR_ramp_changed)

        mR_dwell=tk.StringVar(value = INIT_MR_DWELL)
        mR_dwell.trace("w", mR_dwell_changed)

        mixRinseLabel_Volume = tk.Label(mixRinseFrame, text = "Volume")
        mixRinseLabel_Volume.grid(row=1, column=0, rowspan = 4, pady = 10)
        mixRinseInput_Volume = tk.Entry(mixRinseFrame, width=8, borderwidth=5, textvariable = mR_volume)
        mixRinseInput_Volume.grid(row=1, column=50, rowspan = 4, pady = 10)
        mixRinseLabel_uL = tk.Label(mixRinseFrame, text = "uL")
        mixRinseLabel_uL.grid(row=1, column=100, rowspan = 4, pady = 10)

        mixRinseLabel_Speed = tk.Label(mixRinseFrame, text = "Speed")
        mixRinseLabel_Speed.grid(row=10, column=0, rowspan = 4, pady = 10)
        mixRinseInput_Speed = tk.Entry(mixRinseFrame, width=8, borderwidth=5, textvariable = mR_speed)
        mixRinseInput_Speed.grid(row=10, column=50, rowspan = 4, pady = 10)
        mixRinseLabel_SpeedUnit = tk.Label(mixRinseFrame, text = "0...2047")
        mixRinseLabel_SpeedUnit.grid(row=10, column=100, rowspan = 4, pady = 10)

        mixRinseLabel_SpeedCalculated = tk.Label(mixRinseFrame, text = "Speed")
        mixRinseLabel_SpeedCalculated.grid(row=20, column=0, rowspan = 4, pady = 10)
        mixRinseInput_SpeedCalculated = tk.Entry(mixRinseFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        mixRinseInput_SpeedCalculated.grid(row=20, column=50, rowspan = 4, pady = 10)
        mixRinseLabel_SpeedCalculatedUnit = tk.Label(mixRinseFrame, text = "uL/s")
        mixRinseLabel_SpeedCalculatedUnit.grid(row=20, column=100, rowspan = 4, pady = 10)

        mixRinseLabel_ramp = tk.Label(mixRinseFrame, text = "Ramp")
        mixRinseLabel_ramp.grid(row=30, column=0, rowspan = 4, pady = 10)
        mixRinseInput_ramp = tk.Entry(mixRinseFrame, width=8, borderwidth=5, textvariable = mR_ramp)
        mixRinseInput_ramp.grid(row=30, column=50, rowspan = 4, pady = 10)
        mixRinseLabel_rampUnit = tk.Label(mixRinseFrame, text = "0...2047")
        mixRinseLabel_rampUnit.grid(row=30, column=100, rowspan = 4, pady = 10)

        mixRinseLabel_rampCalculated = tk.Label(mixRinseFrame, text = "Ramp")
        mixRinseLabel_rampCalculated.grid(row=40, column=0, rowspan = 4, pady = 10)
        mixRinseInput_rampCalculated = tk.Entry(mixRinseFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        mixRinseInput_rampCalculated.grid(row=40, column=50, rowspan = 4, pady = 10)
        mixRinseLabel_rampCalculatedUnit = tk.Label(mixRinseFrame, text = "%")
        mixRinseLabel_rampCalculatedUnit.grid(row=40, column=100, rowspan = 4, pady = 10)

        mixRinseLabel_dwell = tk.Label(mixRinseFrame, text = "Dwell")
        mixRinseLabel_dwell.grid(row=50, column=0, rowspan = 4, pady = 10)
        mixRinseInput_dwell = tk.Entry(mixRinseFrame, width=8, borderwidth=5, textvariable = mR_dwell)
        mixRinseInput_dwell.grid(row=50, column=50, rowspan = 4, pady = 10)
        mixRinseLabel_dwellUnit = tk.Label(mixRinseFrame, text = "s")
        mixRinseLabel_dwellUnit.grid(row=50, column=100, rowspan = 4, pady = 10)

        mixRinseTrigger = tk.Button(mixRinseFrame, width=buttonWidth, text ="Manual", command = pumpControl.mixRinseControl)
        mixRinseTrigger.grid(row=60, column=50, pady = 10)

        mixrinsetemp = tk.Label(mixRinseFrame, text = "AUTO INPUTS: 101")
        mixrinsetemp.grid(row=70, column=50, rowspan = 4, pady = 2)


        ################################################################################
        # setup the air gap frame
        ################################################################################
        wholeFrameTracking_x += 200
        airgapFrame = ttk.Labelframe(horizontal_pane, text = "AIR GAP", labelanchor = 'n')
        airgapFrame.columnconfigure(0, weight=1)
        airgapFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(airgapFrame, weight=1)
        airgapFrame.place(x=wholeFrameTracking_x, y=wholeFrameTracking_y)


        # Define strings and then use the trace command to link the callback function 
        # which will get called anytime the text entry field changes

        aG_volume=tk.StringVar(value = INIT_AG_VOL)
        aG_volume.trace("w", aG_volume_changed)

        aG_speed=tk.StringVar(value = INIT_AG_SPEED)
        aG_speed.trace("w", aG_speed_changed)

        aG_ramp=tk.StringVar(value = INIT_AG_RAMP)
        aG_ramp.trace("w", aG_ramp_changed)

        aG_dwell=tk.StringVar(value = INIT_AG_DWELL)
        aG_dwell.trace("w", aG_dwell_changed)

        airgapLabel_Volume = tk.Label(airgapFrame, text = "Volume")
        airgapLabel_Volume.grid(row=1, column=0, rowspan = 4, pady = 10)
        airgapInput_Volume = tk.Entry(airgapFrame, width=8, borderwidth=5, textvariable = aG_volume)
        airgapInput_Volume.grid(row=1, column=50, rowspan = 4, pady = 10)
        airgapLabel_uL = tk.Label(airgapFrame, text = "uL")
        airgapLabel_uL.grid(row=1, column=100, rowspan = 4, pady = 10)

        airgapLabel_Speed = tk.Label(airgapFrame, text = "Speed")
        airgapLabel_Speed.grid(row=10, column=0, rowspan = 4, pady = 10)
        airgapInput_Speed = tk.Entry(airgapFrame, width=8, borderwidth=5, textvariable = aG_speed)
        airgapInput_Speed.grid(row=10, column=50, rowspan = 4, pady = 10)
        airgapLabel_SpeedUnit = tk.Label(airgapFrame, text = "0...2047")
        airgapLabel_SpeedUnit.grid(row=10, column=100, rowspan = 4, pady = 10)

        airgapLabel_SpeedCalculated = tk.Label(airgapFrame, text = "Speed")
        airgapLabel_SpeedCalculated.grid(row=20, column=0, rowspan = 4, pady = 10)
        airgapInput_SpeedCalculated = tk.Entry(airgapFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        airgapInput_SpeedCalculated.grid(row=20, column=50, rowspan = 4, pady = 10)
        airgapLabel_SpeedCalculatedUnit = tk.Label(airgapFrame, text = "uL/s")
        airgapLabel_SpeedCalculatedUnit.grid(row=20, column=100, rowspan = 4, pady = 10)

        airgapLabel_ramp = tk.Label(airgapFrame, text = "Ramp")
        airgapLabel_ramp.grid(row=30, column=0, rowspan = 4, pady = 10)
        airgapInput_ramp = tk.Entry(airgapFrame, width=8, borderwidth=5, textvariable = aG_ramp)
        airgapInput_ramp.grid(row=30, column=50, rowspan = 4, pady = 10)
        airgapLabel_rampUnit = tk.Label(airgapFrame, text = "0...2047")
        airgapLabel_rampUnit.grid(row=30, column=100, rowspan = 4, pady = 10)

        airgapLabel_rampCalculated = tk.Label(airgapFrame, text = "Ramp")
        airgapLabel_rampCalculated.grid(row=40, column=0, rowspan = 4, pady = 10)
        airgapInput_rampCalculated = tk.Entry(airgapFrame, width=8, borderwidth=5, textvariable = tk.StringVar(value = '0'), background='grey')
        airgapInput_rampCalculated.grid(row=40, column=50, rowspan = 4, pady = 10)
        airgapLabel_rampCalculatedUnit = tk.Label(airgapFrame, text = "%")
        airgapLabel_rampCalculatedUnit.grid(row=40, column=100, rowspan = 4, pady = 10)

        airgapLabel_dwell = tk.Label(airgapFrame, text = "Dwell")
        airgapLabel_dwell.grid(row=50, column=0, rowspan = 4, pady = 10)
        airgapInput_dwell = tk.Entry(airgapFrame, width=8, borderwidth=5, textvariable = aG_dwell)
        airgapInput_dwell.grid(row=50, column=50, rowspan = 4, pady = 10)
        airgapLabel_dwellUnit = tk.Label(airgapFrame, text = "s")
        airgapLabel_dwellUnit.grid(row=50, column=100, rowspan = 4, pady = 10)

        airgapTrigger = tk.Button(airgapFrame, width=buttonWidth, text ="Manual", command = pumpControl.airgapControl)
        airgapTrigger.grid(row=60, column=50, pady = 10)

        airgaptemp = tk.Label(airgapFrame, text = "AUTO INPUTS: 011")
        airgaptemp.grid(row=70, column=50, rowspan = 4, pady = 2)

        ##########################################################################################
        # debug for commands
        commandFrame = ttk.Labelframe(horizontal_pane, text="Manual Commands")
        commandFrame.columnconfigure(0, weight=1)
        commandFrame.rowconfigure(0, weight=1)
        horizontal_pane.add(commandFrame, weight=1)
        commandFrame.place(x=0, y=DEBUG_CMD_FRAME_HEIGHT)
        #initializationTrigger = tk.Button(commandFrame, width=15, text ="MC Init Sequence", command = pumpControl.initalizeMotorController)
        #initializationTrigger.grid(row=10, column=1, rowspan = 4, padx = 10, pady = 10)
        busyTriggerToggle = tk.Button(commandFrame, width=15, text ="Busy Toggle", command = pumpControl.debugControlOfBusyIO)
        busyTriggerToggle.grid(row=20, column=1, rowspan = 4, padx = 10, pady = 10)
        #############################################################################################

            

        root.protocol("WM_DELETE_WINDOW", control.on_closing)
        # set up our own main loop
        # will execute taskLoop every 50 millisecond
        root.after(1, control.taskLoop)
        root.mainloop()

    except:
        # need to get this to work so when there is an runtime error, 
        # the serial port isnt kept occupied and need to be unplugged
        sys.exit()
        # control.stopProgram()
