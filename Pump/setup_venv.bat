
SET "StartDir=%cd%"**

:: Version 1.05
:: Clear existing virtual environment to ensure everything is correct
rmdir venv /s /q

:: Install virtual environment and enable it
python -m venv venv
call venv/Scripts/activate

:: Install all packages
easy_install -U pip
pip install numpy
pip install pyserial
pip install serial
pip install ScrolledText
pip install logging
pip install python-tk
pip install pyinstaller
pip install threading
=======


ECHO =======================
ECHO  Python Setup Complete
ECHO =======================
ECHO Note: Type 'deactivate' to exit Python Virtural Environment

pause
